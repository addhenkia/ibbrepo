package Com.IBB.CallCenterAddLead;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

import Com.IBB.DealerManageStockSection.DataProviderForManageStock;
import Com.IBB.DealerToolSection.DataProviderforDealerTools;
import Com.IBB.TestBase.TestBase;

public class AddLead extends TestBase {
//Test 04
	@Test(dataProvider = "DP_AddLead", dataProviderClass = DataProviderForCallCenter.class)
	public void validateAddLeadForm(String Zohoid, String LeadRemark, String LeadTags, String LeadType, String Name,
			String Phoneno, String Email, String State, String City, String MfgYear, String MfgMonth, String Make,
			String Model, String Variant, String Colour, String Kilometer, String Owner, String RegNumber,
			String RegCity, String RegYear, String RegMonth, String Insurance, String InsuranceExpiry,
			String LeadStatus, String ExpectedPrice, String Comments) throws InterruptedException {
		callcenterLogin();
		explicitWait_object(CallCenter_OR.getProperty("btnAddLead"));
		clickOnElement(CallCenter_OR.getProperty("btnAddLead"));
		windowHandling();
		explicitWait_object(CallCenter_OR.getProperty("Zohoid"));
		sendInput(CallCenter_OR.getProperty("Zohoid"), Zohoid);
		getObject(CallCenter_OR.getProperty("leadRemark")).sendKeys(Keys.TAB);
		sendInput(CallCenter_OR.getProperty("leadRemark"), LeadRemark);
		// Lead tags
		getObject(CallCenter_OR.getProperty("leadTagsdrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("leadTagsdrop"));
		selectOption(CallCenter_OR.getProperty("leadTagsdrop"), LeadTags);
		// Lead Type
		getObject(CallCenter_OR.getProperty("leadTypedrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("leadTypedrop"));
		selectOption(CallCenter_OR.getProperty("leadTypedrop"), LeadType);
		if (LeadType.equalsIgnoreCase("Procurement")) {
			addLead(LeadType, Name, Phoneno, Email, State, City, MfgYear, MfgMonth, Make, Model, Variant, Colour,
					Kilometer, Owner, RegNumber, RegCity, RegYear, RegMonth, Insurance, InsuranceExpiry, LeadStatus,
					ExpectedPrice, Comments);

			explicitWait_object(CallCenter_OR.getProperty("successmesg"));
			Assert.assertEquals(getObject(CallCenter_OR.getProperty("successmesg")).isDisplayed(), true,
					"Procurement Lead is not create");
			procurementLeadCallCenter(LeadType, Name, Phoneno, Email, State, City, Make, Model, Variant, Colour,
					Kilometer, Owner, RegNumber);
			clickOnElement(RoadPrice_OR.getProperty("procurmentLeadTab"));
			sendInput(RoadPrice_OR.getProperty("procurementEmail"), CONFIG.getProperty("websiteUser"));
		} else if (LeadType.equalsIgnoreCase("AutoInspekt") || LeadType.equalsIgnoreCase("TAPP")
				|| LeadType.equalsIgnoreCase("Warranty") || LeadType.equalsIgnoreCase("Finance")
				|| LeadType.equalsIgnoreCase("Insurance") || LeadType.equalsIgnoreCase("Road Side Assistance")) {

			addLead(LeadType, Name, Phoneno, Email, State, City, MfgYear, MfgMonth, Make, Model, Variant, Colour,
					Kilometer, Owner, RegNumber, RegCity, RegYear, RegMonth, Insurance, InsuranceExpiry, LeadStatus,
					ExpectedPrice, Comments);
			explicitWait_object(CallCenter_OR.getProperty("successmesg"));

			Assert.assertEquals(getObject(CallCenter_OR.getProperty("successmesg")).isDisplayed(), true,
					"Product Lead is not create");
			productLeadCallCenter(LeadTags, LeadType, Name, Phoneno, Email, State, City, Make, Model, Variant, Colour,
					Kilometer);
		} else {
			addLead(LeadType, Name, Phoneno, Email, State, City, MfgYear, MfgMonth, Make, Model, Variant, Colour,
					Kilometer, Owner, RegNumber, RegCity, RegYear, RegMonth, Insurance, InsuranceExpiry, LeadStatus,
					ExpectedPrice, Comments);
			explicitWait_object(CallCenter_OR.getProperty("successmesg"));
			Assert.assertEquals(getObject(CallCenter_OR.getProperty("successmesg")).isDisplayed(), true,
					"Procurement Lead is not create");
			otherLeadCallCenter(LeadType, Name, Phoneno, Email, State, City, Make, Model, Variant, Colour, Kilometer);

		}

	}

	@Test(dependsOnMethods = "validateAddLeadForm")
	public void validateMandetoryMsg() {

		explicitWait_object(CallCenter_OR.getProperty("btnAddLead"));
		clickOnElement(CallCenter_OR.getProperty("btnAddLead"));
		windowHandling();
		explicitWait_object(CallCenter_OR.getProperty("btnAdd"));
		clickOnElement(CallCenter_OR.getProperty("btnAdd"));
		explicitWait_object(CallCenter_OR.getProperty("remarkValidationMsg"));
		Assert.assertEquals(getObject(CallCenter_OR.getProperty("remarkValidationMsg")).isDisplayed(), true,
				"Mandetary Meassage not display for Remark");
		explicitWait_object(CallCenter_OR.getProperty("leadTypeValidationMsg"));
		Assert.assertEquals(getObject(CallCenter_OR.getProperty("leadTypeValidationMsg")).isDisplayed(), true,
				"Mandetary Meassage not display for Remark");
		explicitWait_object(CallCenter_OR.getProperty("phNumberValidation"));
		Assert.assertEquals(getObject(CallCenter_OR.getProperty("phNumberValidation")).isDisplayed(), true,
				"Mandetary Meassage not display for Remark");
		explicitWait_object(CallCenter_OR.getProperty("emailIdVaidationMsg"));
		Assert.assertEquals(getObject(CallCenter_OR.getProperty("emailIdVaidationMsg")).isDisplayed(), true,
				"Mandetary Meassage not display for Remark");
		explicitWait_object(CallCenter_OR.getProperty("stateValidationMsg"));
		Assert.assertEquals(getObject(CallCenter_OR.getProperty("stateValidationMsg")).isDisplayed(), true,
				"Mandetary Meassage not display for Remark");
		driver.navigate().refresh();
	}

	@Test(dataProvider = "DP_MobNo", dataProviderClass = DataProviderForCallCenter.class, dependsOnMethods = "validateMandetoryMsg")
	public void validateMobileNo(String Mob, String Mob1, String Mob2, String Mob3) throws InterruptedException {
		explicitWait_object(CallCenter_OR.getProperty("btnAddLead"));
		clickOnElement(CallCenter_OR.getProperty("btnAddLead"));
		windowHandling();
		verifyMobNo(Mob);
		Thread.sleep(1000);
		verifyMobNo(Mob1);
		Thread.sleep(1000);
		verifyMobNo(Mob2);
		Thread.sleep(1000);
		verifyMobNo(Mob3);
		driver.navigate().refresh();
	}

	@Test(dataProvider = "DP_EmailID", dataProviderClass = DataProviderForCallCenter.class, dependsOnMethods = "validateMobileNo")
	public void validateEmailID(String Email, String Email1, String Email2, String Email3, String Email4,
			String ValidationMsg, String ValidationMsg1) throws InterruptedException {

		explicitWait_object(CallCenter_OR.getProperty("btnAddLead"));
		clickOnElement(CallCenter_OR.getProperty("btnAddLead"));
		windowHandling();

		verifyEmailID(Email, ValidationMsg);
		VerifyInvalidEMail(Email1, ValidationMsg1);
		Thread.sleep(1000);
		VerifyInvalidEMail(Email2, ValidationMsg1);
		Thread.sleep(1000);
		VerifyInvalidEMail(Email3, ValidationMsg1);
		Thread.sleep(1000);
		VerifyInvalidEMail(Email4, ValidationMsg1);
		driver.navigate().refresh();
		Thread.sleep(5000);
		callcenterLogout();
		Thread.sleep(9000);
	}

	// Dealer Tool Section- Vin Check link

	@Test(dataProvider = "DP_Vincheck", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "validateEmailID")
	public void DealerToolsectionvincheck(String Chassisno) {
		dealerLogin();
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));
		// Verify sub menu tabs
		String subTabOffloadVehicle = getObject(ToolSection_OR.getProperty("subTabOffloadVehicle")).getText();
		Assert.assertEquals(subTabOffloadVehicle, "Offload Vehicles");

		String subTabVincheck = getObject(ToolSection_OR.getProperty("vinchecklink")).getText();
		Assert.assertEquals(subTabVincheck, "Vin Check");

		String subTabresiduals = getObject(ToolSection_OR.getProperty("residulalink")).getText();
		Assert.assertEquals(subTabresiduals, "Residuals");

		String subTabViewDemand = getObject(ToolSection_OR.getProperty("subTabViewDemand")).getText();
		Assert.assertEquals(subTabViewDemand, "View Demand");

		String subTabCheckIBBPrice = getObject(ToolSection_OR.getProperty("subTabCheckIBBPrice")).getText();
		Assert.assertEquals(subTabCheckIBBPrice, "Check IBB Price");

		String subTabwindowStickerlink = getObject(ToolSection_OR.getProperty("windowStickerlink")).getText();
		Assert.assertEquals(subTabwindowStickerlink, "Window Sticker");

		searchVincheck(Chassisno);

	}

	@Test(dependsOnMethods = "DealerToolsectionvincheck")

	public void validateResidualMandetoryMsg() {

		navigateToResidualsTab();
		clickOnElement(ToolSection_OR.getProperty("btnSubmit"));
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualYearMdg")).isDisplayed(), true,
				"Validation message not display for Residual Year");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualLocationMsg")).isDisplayed(), true,
				"Validation message not display for Residual Location");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualKillometerMsg")).isDisplayed(), true,
				"Validation message not display for Residual Killometer");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualOwner")).isDisplayed(), true,
				"Validation message not display for Residual Owner");
		driver.navigate().refresh();
	}

	// Dealer Tool Section- Residuals Link

	@Test(dataProvider = "DP_Residual", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "validateResidualMandetoryMsg")
	public void DealerToolsectionresidual(String MfgeYear, String MfgMonth, String Location, String Make, String Model,
			String Variant, String Colour, String Kilometer, String Owner) throws InterruptedException {

		navigateToResidualsTab();
		// Select Year
		explicitWait_object(ToolSection_OR.getProperty("mfgYearResidual"));
		selectOption(ToolSection_OR.getProperty("mfgYearResidual"), MfgeYear);
		Thread.sleep(1000);
		// Select Month
		explicitWait_object(ToolSection_OR.getProperty("mfgMonthResidual"));
		selectOption(ToolSection_OR.getProperty("mfgMonthResidual"), MfgMonth);
		// Select Location
		explicitWait_object(ToolSection_OR.getProperty("locationResidual"));
		selectOption(ToolSection_OR.getProperty("locationResidual"), Location);
		// Select Make
		explicitWait_object(ToolSection_OR.getProperty("dropMake"));
		selectOption(ToolSection_OR.getProperty("dropMake"), Make);
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("dropModel"));
		selectOption(ToolSection_OR.getProperty("dropModel"), Model);
		// Select Variant
		explicitWait_object(ToolSection_OR.getProperty("dropVariant"));
		selectOption(ToolSection_OR.getProperty("dropVariant"), Variant);
		// Select Colour
		explicitWait_object(ToolSection_OR.getProperty("dropColour"));
		selectOption(ToolSection_OR.getProperty("dropColour"), Colour);
		sendInput(ToolSection_OR.getProperty("kiloResidual"), Kilometer);
		// Select Owner
		explicitWait_object(ToolSection_OR.getProperty("dropOwner"));
		selectOption(ToolSection_OR.getProperty("dropOwner"), Owner);
		clickOnElement(ToolSection_OR.getProperty("btnSubmit"));
		String getcompleteText = Make + " " + Model + " " + Variant + " " + "-" + " " + "January" + " " + MfgeYear + " "
				+ "-" + " " + Location;

		// FIAT LINEA ACTIVE 1.3 -January 2016- MUMBAI
		// MARUTI SUZUKI ALTO 800 LX - January 2016 - MUMBAI

		String getResuidals = getObject(ToolSection_OR.getProperty("getresuidalsText")).getText();
		Assert.assertEquals(getResuidals, getcompleteText);
	}

	// Dealer Tool Section View Demand link

	@Test(dataProvider = "DP_ViewDemand", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionresidual")
	public void DealerToolsectionviewdemand(String Make, String Model) {

		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("viewDemandlink"));
		clickOnElement(ToolSection_OR.getProperty("viewDemandlink"));
		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		Assert.assertEquals(getText, "VIEW DEMAND");
		clickOnElement(ToolSection_OR.getProperty("btnSubmitdemand"));
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("demandMakeMsg")).isDisplayed(), true,
				"Validation message not display for Demand Make");

		// Select Make
		explicitWait_object(ToolSection_OR.getProperty("makeDrop"));
		selectOption(ToolSection_OR.getProperty("makeDrop"), Make);
		clickOnElement(ToolSection_OR.getProperty("btnSubmitdemand"));
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("demandModelMsg")).isDisplayed(), true,
				"Validation message not display for Demand Model");
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("modelDrop"));
		selectOption(ToolSection_OR.getProperty("modelDrop"), Model);
		explicitWait_object(ToolSection_OR.getProperty("btnSubmitdemand"));
		clickOnElement(ToolSection_OR.getProperty("btnSubmitdemand"));

		String getDemandText = getObject(ToolSection_OR.getProperty("viewDemand")).getText();
		String expectedDemand = Make + " " + Model;
		Assert.assertEquals(getDemandText, expectedDemand);

	}

	// Dealer Tool Section Price Check link

	@Test(dataProvider = "DP_PriceCheck", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionviewdemand")
	public void DealerToolsectionpricecheck(String Location, String MfgeYear, String MfgMonth, String Make,
			String Model, String Variant, String Colour, String Kilometer, String Owner) throws InterruptedException {
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("priceChecklink"));
		clickOnElement(ToolSection_OR.getProperty("priceChecklink"));

		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		Assert.assertEquals(getText, "CHECK IBB PRICE");

		explicitWait_object(ToolSection_OR.getProperty("btnSubmitprice"));
		clickOnElement(ToolSection_OR.getProperty("btnSubmitprice"));

		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualYearMdg")).isDisplayed(), true,
				"Validation message not display for Mang Year");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualLocationMsg")).isDisplayed(), true,
				"Validation message not display for Mang Location");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualKillometerMsg")).isDisplayed(), true,
				"Validation message not display for  Killometer");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualOwner")).isDisplayed(), true,
				"Validation message not display for Owner");
		// Select Location
		Thread.sleep(5000);
		explicitWait_object(ToolSection_OR.getProperty("locationPrice"));
		selectOption(ToolSection_OR.getProperty("locationPrice"), Location);
		// Select Year
		explicitWait_object(ToolSection_OR.getProperty("mfgYearPrice"));
		selectOption(ToolSection_OR.getProperty("mfgYearPrice"), MfgeYear);
		// Select Month
		explicitWait_object(ToolSection_OR.getProperty("mfgMonthPrice"));
		selectOption(ToolSection_OR.getProperty("mfgMonthPrice"), MfgMonth);
		// Select Make
		explicitWait_object(ToolSection_OR.getProperty("makePrice"));
		selectOption(ToolSection_OR.getProperty("makePrice"), Make);
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("modelPrice"));
		selectOption(ToolSection_OR.getProperty("modelPrice"), Model);
		// Select Variant
		explicitWait_object(ToolSection_OR.getProperty("variantPrice"));
		selectOption(ToolSection_OR.getProperty("variantPrice"), Variant);
		// Select Color
		explicitWait_object(ToolSection_OR.getProperty("colourPrice"));
		selectOption(ToolSection_OR.getProperty("colourPrice"), Colour);
		sendInput(ToolSection_OR.getProperty("kilometerPrice"), Kilometer);
		// Select Owner
		explicitWait_object(ToolSection_OR.getProperty("ownerPrice"));
		selectOption(ToolSection_OR.getProperty("ownerPrice"), Owner);
		clickOnElement(ToolSection_OR.getProperty("btnSubmitprice"));
		String actualColor = Colour.substring(0, 1).toLowerCase() + Colour.substring(1);
		String getcompleteText = Make + " " + Model + " " + Variant + " " + "-" + " " + actualColor + " " + "-" + " "
				+ "January" + " " + MfgeYear + " " + "-" + " " + Location;

		// FIAT LINEA ACTIVE 1.3 - red - January 2016 - MUMBAI
		String getPriceDetail = getObject(ToolSection_OR.getProperty("getPricedetailstext")).getText();
		Assert.assertEquals(getPriceDetail, getcompleteText);
	}

	// Dealer Tool Section window sticker link

	@Test(dataProvider = "DP_WindowSticker", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionpricecheck")
	public void DealerToolsectionwindowsticker(String MfgeYear, String MfgMonth, String Fueltype, String Make,
			String Model, String Variant, String Transmission, String Colour, String Kilometer, String Owner,
			String Location, String RegiNo, String ExptPrice) {

		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("windowStickerlink"));
		clickOnElement(ToolSection_OR.getProperty("windowStickerlink"));

		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		Assert.assertEquals(getText, "WINDOW STICKER");

		explicitWait_object(ToolSection_OR.getProperty("btnSubWindow"));
		clickOnElement(ToolSection_OR.getProperty("btnSubWindow"));

		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualYearMdg")).isDisplayed(), true,
				"Validation message not display for window Year");

		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowFuelMdg")).isDisplayed(), true,
				"Validation message not display for Fuel Type");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowTransmittionMsg")).isDisplayed(), true,
				"Validation message not display for Transmittion Type");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualKillometerMsg")).isDisplayed(), true,
				"Validation message not display for  Killometer");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualOwner")).isDisplayed(), true,
				"Validation message not display for Owner");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualLocationMsg")).isDisplayed(), true,
				"Validation message not display for window Location");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowRegNoMsg")).isDisplayed(), true,
				"Validation message not display for window Registration No");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowExpectedPrice")).isDisplayed(), true,
				"Validation message not display for window Expected Price");
		// Select Year
		explicitWait_object(ToolSection_OR.getProperty("mfgYearWindow"));
		selectOption(ToolSection_OR.getProperty("mfgYearWindow"), MfgeYear);
		// Select Month
		explicitWait_object(ToolSection_OR.getProperty("mfgMonthWindow"));
		selectOption(ToolSection_OR.getProperty("mfgMonthWindow"), MfgMonth);
		// Select Fuel Type
		explicitWait_object(ToolSection_OR.getProperty("fueltypeWindow"));
		selectOption(ToolSection_OR.getProperty("fueltypeWindow"), Fueltype);
		// Select Fuel Type
		explicitWait_object(ToolSection_OR.getProperty("makeWindow"));
		selectOption(ToolSection_OR.getProperty("makeWindow"), Make);
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("modelWindow"));
		selectOption(ToolSection_OR.getProperty("modelWindow"), Model);
		// Select Variant
		explicitWait_object(ToolSection_OR.getProperty("variantWindow"));
		selectOption(ToolSection_OR.getProperty("variantWindow"), Variant);
		// Select Transmission
		explicitWait_object(ToolSection_OR.getProperty("transmnWindow"));
		selectOption(ToolSection_OR.getProperty("transmnWindow"), Transmission);
		// Select Colour
		explicitWait_object(ToolSection_OR.getProperty("colourWindow"));
		selectOption(ToolSection_OR.getProperty("colourWindow"), Colour);

		sendInput(ToolSection_OR.getProperty("kilometerPrice"), Kilometer);
		// Select Owner
		explicitWait_object(ToolSection_OR.getProperty("ownerPrice"));
		selectOption(ToolSection_OR.getProperty("ownerPrice"), Owner);
		// Select Location
		explicitWait_object(ToolSection_OR.getProperty("locationWindow"));
		selectOption(ToolSection_OR.getProperty("locationWindow"), Location);
		sendInput(ToolSection_OR.getProperty("reginoWindow"), RegiNo);

		sendInput(ToolSection_OR.getProperty("exptprcWindow"), ExptPrice);
		clickOnElement(ToolSection_OR.getProperty("sellingprice"));
		String parentHandle = driver.getWindowHandle();
		clickOnElement(ToolSection_OR.getProperty("btnSubWindow"));
		windowHandling();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

		}
		String getText1 = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/h3")).getText();
		Assert.assertEquals(getText1, "test dealer");
		driver.close();
		driver.switchTo().window(parentHandle);

	}

	@Test(dataProvider = "DP_WindowSticker", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionpricecheck")
	public void dealerOffloadVehicles(String MfgeYear, String MfgMonth, String Fueltype, String Make, String Model,
			String Variant, String Transmission, String Colour, String Kilometer, String Owner, String Location,
			String RegiNo, String ExptPrice) {
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("subTabOffloadVehicle"));
		clickOnElement(ToolSection_OR.getProperty("subTabOffloadVehicle"));

		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		Assert.assertEquals(getText, "OFFLOAD VEHICLES");
		// explicitWait_object(StockSection_OR.getProperty("btnAddOfLeadStock"));
		// clickOnElement(StockSection_OR.getProperty("btnAddOfLeadStock"));
	}

	@Test(dataProvider = "DP_offLoadManageStock", dataProviderClass = DataProviderForManageStock.class, dependsOnMethods = "dealerOffloadVehicles")
	// @Test(dataProvider = "DP_ManageStock", dataProviderClass =
	// DataProviderForManageStock.class)
	public void dealeroffLoadStockSection(String StockStatus, String StockCategory, String MfgYear, String MfgMonth,
			String Make, String Model, String Variant, String Colour, String Kilometer, String Owner, String RegNumber,
			String RegCity, String RegYear, String RegMonth, String Insurance, String InsuranceExpiry, String ChassisNo,
			String EngineNo, String CertifiedBy, String AboutStock, String SellingPrice, String DealerPrice)
					throws InterruptedException {
		// dealerLogin();
		// Add New Stock
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));
		explicitWait_object(ToolSection_OR.getProperty("subTabOffloadVehicle"));
		clickOnElement(ToolSection_OR.getProperty("subTabOffloadVehicle"));
		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		Assert.assertEquals(getText, "OFFLOAD VEHICLES");
		explicitWait_object(ToolSection_OR.getProperty("btnAddOfLeadStock"));
		clickOnElement(ToolSection_OR.getProperty("btnAddOfLeadStock"));
		windowHandling();
		dealerAddStock(StockStatus, StockCategory, MfgYear, MfgMonth, Make, Model, Variant, Colour, Kilometer, Owner,
				RegNumber, RegCity, RegYear, RegMonth, Insurance, InsuranceExpiry, ChassisNo, EngineNo, CertifiedBy,
				AboutStock, SellingPrice, DealerPrice);
		String getSuccessMsgText = getObject(StockSection_OR.getProperty("addStockMsg")).getText();
		String expectedSuccessMsg = Make + " " + Model + "" + Variant;
		driver.navigate().refresh();
	}

	// Delear Manage stock
	@Test(dependsOnMethods = "dealeroffLoadStockSection")
	public void verifyMenu() throws InterruptedException {
		// dealerLogin();
		verifyLHSMenu();
	}

	@Test(dependsOnMethods = "verifyMenu")
	public void verifyAddStockMandetaryMsg() throws InterruptedException {

		
		clickOnElement(StockSection_OR.getProperty("manageLink"));
		explicitWait_object(StockSection_OR.getProperty("mangestockTitle"));
		String getStockTitle = getObject(StockSection_OR.getProperty("mangestockTitle")).getText();
		Assert.assertEquals(getStockTitle, "MANAGE STOCKS");

		explicitWait_object(StockSection_OR.getProperty("btnAddStock"));
		clickOnElement(StockSection_OR.getProperty("btnAddStock"));
		windowHandling();

		explicitWait_object(StockSection_OR.getProperty("btnviewIBBprice"));
		clickOnElement(StockSection_OR.getProperty("btnviewIBBprice"));
		explicitWait_object(StockSection_OR.getProperty("btnAdd"));
		clickOnElement(StockSection_OR.getProperty("btnAdd"));
		Thread.sleep(5000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scroll(0, -250);");
		// Validation Message
		explicitWait_object(StockSection_OR.getProperty("vehicalType"));
		Assert.assertEquals(getObject(StockSection_OR.getProperty("vehicalType")).isDisplayed(), true,
				"Validation message not display for Vehicle Type");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("stockStatus")).isDisplayed(), true,
				"Validation message not display for Stock Status");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("mfgYear")).isDisplayed(), true,
				"Validation message not display for Manufacture Year");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("mgfMonth")).isDisplayed(), true,
				"Validation message not display for Manufacture Month");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("makeMsg")).isDisplayed(), true,
				"Validation message not display for Make");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("modelMsg")).isDisplayed(), true,
				"Validation message not display for Model");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("variantMsg")).isDisplayed(), true,
				"Validation message not display for Variant");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("variantMsg")).isDisplayed(), true,
				"Validation message not display for Variant");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("kmMndatoryMsg")).isDisplayed(), true,
				"Validation message not display for Kilometer");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("ownerMsg")).isDisplayed(), true,
				"Validation message not display for Owner");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regNoMsg")).isDisplayed(), true,
				"Validation message not display for Registration Number");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regCity")).isDisplayed(), true,
				"Validation message not display for Registration City");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regYear")).isDisplayed(), true,
				"Validation message not display for Registration Year");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regMonth")).isDisplayed(), true,
				"Validation message not display for Registration Month");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("insuranceMsg")).isDisplayed(), true,
				"Validation message not display for Insurance");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("sellingPriceMsg")).isDisplayed(), true,
				"Validation message not display for Selling Price");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("dealergPriceMsg")).isDisplayed(), true,
				"Validation message not display for Dealer Price");
		driver.navigate().refresh();

	}

	// Dealer Stock Section
	@Test(dataProvider = "DP_ManageStock", dataProviderClass = DataProviderForManageStock.class, dependsOnMethods = "verifyAddStockMandetaryMsg")
	// @Test(dataProvider = "DP_ManageStock", dataProviderClass =
	// DataProviderForManageStock.class)
	public void validateDealerAddStock(String StockStatus, String StockCategory, String MfgYear, String MfgMonth,
			String Make, String Model, String Variant, String Colour, String Kilometer, String Owner, String RegNumber,
			String RegCity, String RegYear, String RegMonth, String Insurance, String InsuranceExpiry, String ChassisNo,
			String EngineNo, String CertifiedBy, String AboutStock, String SellingPrice, String DealerPrice)
					throws InterruptedException {
		

		explicitWait_object(StockSection_OR.getProperty("btnAddStock"));
		clickOnElement(StockSection_OR.getProperty("btnAddStock"));
		windowHandling();

		// Add Stock
		dealerAddStock(StockStatus, StockCategory, MfgYear, MfgMonth, Make, Model, Variant, Colour, Kilometer, Owner,
				RegNumber, RegCity, RegYear, RegMonth, Insurance, InsuranceExpiry, ChassisNo, EngineNo, CertifiedBy,
				AboutStock, SellingPrice, DealerPrice);

		explicitWait_object(".//*[@id='container']/div/div[1]/div/div[2]/div/div[1]");
		String getAddedStockMsg = driver.findElement(By.xpath(".//*[@id='container']/div/div[1]/div/div[2]/div/div[1]"))
				.getText();
		System.out.println("getActualMsg is" + getAddedStockMsg);

		/*
		 * String addedStockvalidationMs = Make + " " + Model + " " + Variant +
		 * " " + "successfully added"; System.out.println(
		 * "addedStockvalidationMs is" +addedStockvalidationMs);
		 * Assert.assertEquals(getAddedStockMsg, addedStockvalidationMs);
		 */
		// Search Stock
		Thread.sleep(5000);
		searchMakeStock(Make);
		Thread.sleep(5000);
		searchModelStock(Model);
		Thread.sleep(5000);
		searchVarientStock(Variant);
		Thread.sleep(5000);
		searchUnavaibleStock();
		Thread.sleep(5000);

		explicitWait_object(StockSection_OR.getProperty("regNoField"));
		sendInput(StockSection_OR.getProperty("regNoField"), RegNumber + uniqueID);

	}

	/*
	 * @Test(dataProvider = "DP_SearchStock", dataProviderClass =
	 * DataProviderForManageStock.class, dependsOnMethods =
	 * "validateDealerAddStock") public void verifySearchStock(String Make,
	 * String Model, String Variant) throws InterruptedException {
	 * clickOnElement(StockSection_OR.getProperty("myStockdrop"));
	 * explicitWait_object(StockSection_OR.getProperty("manageLink"));
	 * clickOnElement(StockSection_OR.getProperty("manageLink"));
	 * Thread.sleep(5000); searchMakeStock(Make); Thread.sleep(5000);
	 * searchModelStock(Model); Thread.sleep(5000); searchVarientStock(Variant);
	 * Thread.sleep(5000); searchUnavaibleStock(); }
	 */

	@Test(dataProvider = "DP_EditStock", dataProviderClass = DataProviderForManageStock.class, dependsOnMethods = "validateDealerAddStock")
	public void validateEditStock(String StockStatus, String MfgYear, String MfgMonth, String Make, String Model,
			String Variant, String Colour, String Kilometer, String Owner, String RegNumber, String RegCity,
			String RegYear, String RegMonth, String Insurance, String CertifiedBy, String SellingPrice,
			String DealerPrice) throws InterruptedException {

		Thread.sleep(5000);

		moveToMenu(StockSection_OR.getProperty("btnActions"));
		clickOnElement(StockSection_OR.getProperty("btnEdit"));
		windowHandling();

		clickOnElement(StockSection_OR.getProperty("rdiobtnCommercial"));

		dealerEditStock(StockStatus, MfgYear, MfgMonth, Make, Model, Variant, Colour, Kilometer, Owner, RegNumber,
				RegCity, RegYear, RegMonth, Insurance, CertifiedBy, SellingPrice, DealerPrice);

		String getActualMsg = driver.findElement(By.xpath(".//*[@id='container']//div[1]/div/div[2]/div/div[1]"))
				.getText();
		// String getActualMsg1 = getActualMsg.substring(1);

		String validationMs = Make + " " + Model + " " + Variant + " " + "successfully updated";
		// Assert.assertEquals(getActualMsg1, validationMs);
		// Assert.assertEquals(validationMs.contains(getActualMsg1),true,"Not
		// Susscess");

		explicitWait_object(StockSection_OR.getProperty("regNoField"));
		sendInput(StockSection_OR.getProperty("regNoField"), RegNumber);
	}

	@Test(dataProvider = "DP_Vincheck", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "validateEditStock")
	public void stockToolButton(String Chassisno1) throws InterruptedException {
		Thread.sleep(5000);
		moveToMenu(StockSection_OR.getProperty("btnActions"));
		clickOnElement(StockSection_OR.getProperty("btnTool"));
		windowHandling();

		stockVincheck(Chassisno1);

	}
}
