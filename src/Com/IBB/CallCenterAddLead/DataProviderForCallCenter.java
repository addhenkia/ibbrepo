package Com.IBB.CallCenterAddLead;

import org.testng.annotations.DataProvider;

import Com.IBB.TestBase.TestBase;

public class DataProviderForCallCenter extends TestBase {

	// Call Center-Add Lead
	@DataProvider(name = "DP_AddLead")
	public static Object[][] addlead() throws Exception {
		Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "CallCenter", "AddLead");
		return retObjArr;

	}
	
	// Call Center-Email Verification
		@DataProvider(name = "DP_EmailID")
		public static Object[][] emailID() throws Exception {
			Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "CallCenter", "EmailID");
			return retObjArr;

		}
		
		// Call Center-Mobile Verification
				@DataProvider(name = "DP_MobNo")
				public static Object[][] mobNo() throws Exception {
					Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "CallCenter", "MobNo");
					return retObjArr;

				}
}