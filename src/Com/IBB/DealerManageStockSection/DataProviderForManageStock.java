package Com.IBB.DealerManageStockSection;

import org.testng.annotations.DataProvider;

import Com.IBB.TestBase.TestBase;

public class DataProviderForManageStock  extends TestBase{

		
	// Manage Stock
	@DataProvider(name = "DP_ManageStock")
	public static Object[][] AddStock() throws Exception {
		Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "Dealertool", "ManageStock");
		return retObjArr;
	}
			
	// Edit Stock
	@DataProvider(name = "DP_EditStock")
	public static Object[][] EditStock() throws Exception {
		Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "Dealertool", "EditStock");
		return retObjArr;
			
			
	}
			
	
	@DataProvider(name = "DP_offLoadManageStock")
	public static Object[][] offloadManageStock() throws Exception {
		Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "Dealertool", "offLoadManageStock");
		return retObjArr;

	}
	// Search Stock
	@DataProvider(name = "DP_SearchStock")
	public static Object[][] SearchStock() throws Exception {
		Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "Dealertool", "SearchStock");
		return retObjArr;

	}
	}

	
	
