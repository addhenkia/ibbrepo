package Com.IBB.DealerManageStockSection;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;
import Com.IBB.DealerManageStockSection.DataProviderForManageStock;
import Com.IBB.DealerToolSection.DataProviderforDealerTools;
import Com.IBB.TestBase.TestBase;

public class DealerStcokSection extends TestBase {

	@Test
	public void verifyMenu() throws InterruptedException {
		// dealerLogin();
		verifyLHSMenu();
	}

	@Test(dependsOnMethods = "verifyMenu")
	public void verifyAddStockMandetaryMsg() throws InterruptedException {

		// navigateToMangeStockTab();
		clickOnElement(StockSection_OR.getProperty("manageLink"));
		explicitWait_object(StockSection_OR.getProperty("mangestockTitle"));
		String getStockTitle = getObject(StockSection_OR.getProperty("mangestockTitle")).getText();
		System.out.println("getStockTitle is" + getStockTitle);
		Assert.assertEquals(getStockTitle, "MANAGE STOCKS");

		explicitWait_object(StockSection_OR.getProperty("btnAddStock"));
		clickOnElement(StockSection_OR.getProperty("btnAddStock"));
		windowHandling();

		explicitWait_object(StockSection_OR.getProperty("btnviewIBBprice"));
		clickOnElement(StockSection_OR.getProperty("btnviewIBBprice"));
		explicitWait_object(StockSection_OR.getProperty("btnAdd"));
		clickOnElement(StockSection_OR.getProperty("btnAdd"));
		Thread.sleep(5000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scroll(0, -250);");
		// Validation Message
		explicitWait_object(StockSection_OR.getProperty("vehicalType"));
		String getText = getObject(StockSection_OR.getProperty("vehicalType")).getText();
		System.out.println("getText" + getText);
		Assert.assertEquals(getObject(StockSection_OR.getProperty("vehicalType")).isDisplayed(), true,
				"Validation message not display for Vehicle Type");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("stockStatus")).isDisplayed(), true,
				"Validation message not display for Stock Status");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("mfgYear")).isDisplayed(), true,
				"Validation message not display for Manufacture Year");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("mgfMonth")).isDisplayed(), true,
				"Validation message not display for Manufacture Month");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("makeMsg")).isDisplayed(), true,
				"Validation message not display for Make");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("modelMsg")).isDisplayed(), true,
				"Validation message not display for Model");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("variantMsg")).isDisplayed(), true,
				"Validation message not display for Variant");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("variantMsg")).isDisplayed(), true,
				"Validation message not display for Variant");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("kmMndatoryMsg")).isDisplayed(), true,
				"Validation message not display for Kilometer");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("ownerMsg")).isDisplayed(), true,
				"Validation message not display for Owner");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regNoMsg")).isDisplayed(), true,
				"Validation message not display for Registration Number");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regCity")).isDisplayed(), true,
				"Validation message not display for Registration City");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regYear")).isDisplayed(), true,
				"Validation message not display for Registration Year");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("regMonth")).isDisplayed(), true,
				"Validation message not display for Registration Month");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("insuranceMsg")).isDisplayed(), true,
				"Validation message not display for Insurance");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("sellingPriceMsg")).isDisplayed(), true,
				"Validation message not display for Selling Price");

		Assert.assertEquals(getObject(StockSection_OR.getProperty("dealergPriceMsg")).isDisplayed(), true,
				"Validation message not display for Dealer Price");
		driver.navigate().refresh();

	}

	// Dealer Stock Section
	@Test(dataProvider = "DP_ManageStock", dataProviderClass = DataProviderForManageStock.class, dependsOnMethods = "verifyAddStockMandetaryMsg")
	public void validateDealerAddStock(String StockStatus, String StockCategory, String MfgYear, String MfgMonth,
			String Make, String Model, String Variant, String Colour, String Kilometer, String Owner, String RegNumber,
			String RegCity, String RegYear, String RegMonth, String Insurance, String InsuranceExpiry, String ChassisNo,
			String EngineNo, String CertifiedBy, String AboutStock, String SellingPrice, String DealerPrice)
					throws InterruptedException {
		clickOnElement(StockSection_OR.getProperty("manageLink"));
		explicitWait_object(StockSection_OR.getProperty("mangestockTitle"));
		String getStockTitle = getObject(StockSection_OR.getProperty("mangestockTitle")).getText();
		System.out.println("getStockTitle is" + getStockTitle);
		Assert.assertEquals(getStockTitle, "MANAGE STOCKS");

		explicitWait_object(StockSection_OR.getProperty("btnAddStock"));
		clickOnElement(StockSection_OR.getProperty("btnAddStock"));
		windowHandling();

		// Add Stock
		dealerAddStock(StockStatus, StockCategory, MfgYear, MfgMonth, Make, Model, Variant, Colour, Kilometer, Owner,
				RegNumber, RegCity, RegYear, RegMonth, Insurance, InsuranceExpiry, ChassisNo, EngineNo, CertifiedBy,
				AboutStock, SellingPrice, DealerPrice);

		explicitWait_object(".//*[@id='container']/div/div[1]/div/div[2]/div/div[1]");
		String getAddedStockMsg = driver.findElement(By.xpath(".//*[@id='container']/div/div[1]/div/div[2]/div/div[1]"))
				.getText();
		System.out.println("getActualMsg is" + getAddedStockMsg);

		String addedStockvalidationMs = Make + " " + Model + " " + Variant + " " + "successfully added";
		System.out.println("addedStockvalidationMs is" + addedStockvalidationMs);
		Assert.assertEquals(getAddedStockMsg, addedStockvalidationMs);

		Thread.sleep(5000);
		explicitWait_object(StockSection_OR.getProperty("regNoField"));
		sendInput(StockSection_OR.getProperty("regNoField"), RegNumber + uniqueID);

	}

	@Test(dataProvider = "DP_EditStock", dataProviderClass = DataProviderForManageStock.class, dependsOnMethods = "validateDealerAddStock")
	public void validateEditStock(String StockStatus, String MfgYear, String MfgMonth, String Make, String Model,
			String Variant, String Colour, String Kilometer, String Owner, String RegNumber, String RegCity,
			String RegYear, String RegMonth, String Insurance, String CertifiedBy, String SellingPrice,
			String DealerPrice) throws InterruptedException {

		Thread.sleep(5000);
		moveToMenu(StockSection_OR.getProperty("btnActions"));
		clickOnElement(StockSection_OR.getProperty("btnEdit"));
		windowHandling();

		clickOnElement(StockSection_OR.getProperty("rdiobtnCommercial"));

		dealerEditStock(StockStatus, MfgYear, MfgMonth, Make, Model, Variant, Colour, Kilometer, Owner, RegNumber,
				RegCity, RegYear, RegMonth, Insurance, CertifiedBy, SellingPrice, DealerPrice);

		String getActualMsg = driver.findElement(By.xpath(".//*[@id='container']//div[1]/div/div[2]/div/div[1]"))
				.getText();
		System.out.println("getActualMsg is" + getActualMsg);
		// String getActualMsg1 = getActualMsg.substring(1);

		String validationMs = Make + " " + Model + " " + Variant + " " + "successfully updated";
		System.out.println("validationMs is" + validationMs);
		// Assert.assertEquals(getActualMsg1, validationMs);
		// Assert.assertEquals(validationMs.contains(getActualMsg1),true,"Not
		// Susscess");

		explicitWait_object(StockSection_OR.getProperty("regNoField"));
		sendInput(StockSection_OR.getProperty("regNoField"), RegNumber);
	}

	@Test(dataProvider = "DP_Vincheck", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "validateEditStock")
	public void stockToolButton(String Chassisno1) throws InterruptedException {

		Thread.sleep(5000);
		moveToMenu(StockSection_OR.getProperty("btnActions"));
		clickOnElement(StockSection_OR.getProperty("btnTool"));
		windowHandling();

		stockVincheck(Chassisno1);

		explicitWait_object(StockSection_OR.getProperty("vinCheckTitle"));

	}

	@Test(dataProvider = "DP_SearchStock", dataProviderClass = DataProviderForManageStock.class, dependsOnMethods = "stockToolButton")
	public void verifySearchStock(String Make, String Model, String Variant) throws InterruptedException {
		dealerLogin();
		clickOnElement(StockSection_OR.getProperty("myStockdrop"));
		explicitWait_object(StockSection_OR.getProperty("manageLink"));
		clickOnElement(StockSection_OR.getProperty("manageLink"));
		Thread.sleep(5000);
		searchMakeStock(Make);
		Thread.sleep(5000);
		searchModelStock(Model);
		Thread.sleep(5000);
		searchVarientStock(Variant);
		Thread.sleep(5000);
		searchUnavaibleStock();
	}

	/*
	 * public void filterDate(){
	 * 
	 * clickOnElement(StockSection_OR.getProperty("btnFilter"));
	 * clickOnElement(StockSection_OR.getProperty("startDate"));
	 * clickOnElement(StockSection_OR.getProperty("endDate"));
	 * clickOnElement(StockSection_OR.getProperty("btnSearchFilter"));
	 * 
	 * }
	 */

}
