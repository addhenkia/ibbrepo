package Com.IBB.DealerToolSection;

import org.testng.annotations.DataProvider;

import Com.IBB.TestBase.TestBase;

public class DataProviderforDealerTools extends TestBase{

	//Dealer Vincheck
	
	@DataProvider(name = "DP_Vincheck")
	public static Object[][] dealervincheck() throws Exception
	{
	Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"Dealertool","Vincheck");
	return retObjArr;
	}
			
	//Dealer residual
	
    @DataProvider(name = "DP_Residual")
	public static Object[][] dealerresidual() throws Exception
	{
	Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"Dealertool","Residual");
	return retObjArr;	
	}
			
	//Dealer View Demand
    
	@DataProvider(name = "DP_ViewDemand")
	public static Object[][] dealerviewdemand() throws Exception
	{
	Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"Dealertool","ViewDemand");
	return retObjArr;							
	}
	
	//Dealer Price Check
	
	@DataProvider(name = "DP_PriceCheck")
	public static Object[][] DealerToolsectionpricecheck() throws Exception
	{
	Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"Dealertool","PriceCheck");
	return retObjArr;								
	}
	
	//Dealer Window Sticker
	
	@DataProvider(name = "DP_WindowSticker")
	public static Object[][] DealerToolsectionwindowsticker() throws Exception
	{
	Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"Dealertool","WindowSticker");
	return retObjArr;		
	}
					
}
