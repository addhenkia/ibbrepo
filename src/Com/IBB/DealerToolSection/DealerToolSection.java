package Com.IBB.DealerToolSection;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import Com.IBB.DealerManageStockSection.DataProviderForManageStock;
import Com.IBB.TestBase.TestBase;

public class DealerToolSection extends TestBase {

	// Dealer Tool Section Vin Check link

	@Test(enabled=false,dataProvider = "DP_Vincheck", dataProviderClass = DataProviderforDealerTools.class)
	public void DealerToolsectionvincheck(String Chassisno) {
		dealerLogin();
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));
		// Verify sub menu tabs
		String subTabOffloadVehicle = getObject(ToolSection_OR.getProperty("subTabOffloadVehicle")).getText();
		Assert.assertEquals(subTabOffloadVehicle, "Offload Vehicles");

		String subTabVincheck = getObject(ToolSection_OR.getProperty("vinchecklink")).getText();
		Assert.assertEquals(subTabVincheck, "Vin Check");

		String subTabresiduals = getObject(ToolSection_OR.getProperty("residulalink")).getText();
		Assert.assertEquals(subTabresiduals, "Residuals");

		String subTabViewDemand = getObject(ToolSection_OR.getProperty("subTabViewDemand")).getText();
		Assert.assertEquals(subTabViewDemand, "View Demand");

		String subTabCheckIBBPrice = getObject(ToolSection_OR.getProperty("subTabCheckIBBPrice")).getText();
		Assert.assertEquals(subTabCheckIBBPrice, "Check IBB Price");

		String subTabwindowStickerlink = getObject(ToolSection_OR.getProperty("windowStickerlink")).getText();
		Assert.assertEquals(subTabwindowStickerlink, "Window Sticker");

		searchVincheck(Chassisno);

	}

	@Test(enabled=false,dependsOnMethods = "DealerToolsectionvincheck")

	public void validateResidualMandetoryMsg() {

		navigateToResidualsTab();
		clickOnElement(ToolSection_OR.getProperty("btnSubmit"));
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualYearMdg")).isDisplayed(), true,
				"Validation message not display for Residual Year");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualLocationMsg")).isDisplayed(), true,
				"Validation message not display for Residual Location");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualKillometerMsg")).isDisplayed(), true,
				"Validation message not display for Residual Killometer");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualOwner")).isDisplayed(), true,
				"Validation message not display for Residual Owner");
		driver.navigate().refresh();
	}

	// Dealer Tool Section- Residuals Link

	@Test(enabled=false,dataProvider = "DP_Residual", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "validateResidualMandetoryMsg")
	public void DealerToolsectionresidual(String MfgeYear, String MfgMonth, String Location, String Make, String Model,
			String Variant, String Colour, String Kilometer, String Owner) throws InterruptedException {

		navigateToResidualsTab();
		// Select Year
		explicitWait_object(ToolSection_OR.getProperty("mfgYearResidual"));
		selectOption(ToolSection_OR.getProperty("mfgYearResidual"), MfgeYear);
		Thread.sleep(1000);
		// Select Month
		explicitWait_object(ToolSection_OR.getProperty("mfgMonthResidual"));
		selectOption(ToolSection_OR.getProperty("mfgMonthResidual"), MfgMonth);
		// Select Location
		explicitWait_object(ToolSection_OR.getProperty("locationResidual"));
		selectOption(ToolSection_OR.getProperty("locationResidual"), Location);
		// Select Make
		explicitWait_object(ToolSection_OR.getProperty("dropMake"));
		selectOption(ToolSection_OR.getProperty("dropMake"), Make);
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("dropModel"));
		selectOption(ToolSection_OR.getProperty("dropModel"), Model);
		// Select Variant
		explicitWait_object(ToolSection_OR.getProperty("dropVariant"));
		selectOption(ToolSection_OR.getProperty("dropVariant"), Variant);
		// Select Colour
		explicitWait_object(ToolSection_OR.getProperty("dropColour"));
		selectOption(ToolSection_OR.getProperty("dropColour"), Colour);
		sendInput(ToolSection_OR.getProperty("kiloResidual"), Kilometer);
		// Select Owner
		explicitWait_object(ToolSection_OR.getProperty("dropOwner"));
		selectOption(ToolSection_OR.getProperty("dropOwner"), Owner);
		clickOnElement(ToolSection_OR.getProperty("btnSubmit"));
		String getcompleteText = Make + " " + Model + " " + Variant + " " + "-" + " " + "January" + " " + MfgeYear + " "
				+ "-" + " " + Location;

		// FIAT LINEA ACTIVE 1.3 -January 2016- MUMBAI
		// MARUTI SUZUKI ALTO 800 LX - January 2016 - MUMBAI

		System.out.println("getcompleteText" + getcompleteText);
		String getResuidals = getObject(ToolSection_OR.getProperty("getresuidalsText")).getText();
		System.out.println("getResuidals" + getResuidals);
		Assert.assertEquals(getResuidals, getcompleteText);
	}

	// Dealer Tool Section View Demand link

	@Test(enabled=false,dataProvider = "DP_ViewDemand", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionresidual")
	public void DealerToolsectionviewdemand(String Make, String Model) {

		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("viewDemandlink"));
		clickOnElement(ToolSection_OR.getProperty("viewDemandlink"));
		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		Assert.assertEquals(getText, "VIEW DEMAND");
		clickOnElement(ToolSection_OR.getProperty("btnSubmitdemand"));
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("demandMakeMsg")).isDisplayed(), true,
				"Validation message not display for Demand Make");

		// Select Make
		explicitWait_object(ToolSection_OR.getProperty("makeDrop"));
		selectOption(ToolSection_OR.getProperty("makeDrop"), Make);
		clickOnElement(ToolSection_OR.getProperty("btnSubmitdemand"));
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("demandModelMsg")).isDisplayed(), true,
				"Validation message not display for Demand Model");
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("modelDrop"));
		selectOption(ToolSection_OR.getProperty("modelDrop"), Model);
		explicitWait_object(ToolSection_OR.getProperty("btnSubmitdemand"));
		clickOnElement(ToolSection_OR.getProperty("btnSubmitdemand"));

		String getDemandText = getObject(ToolSection_OR.getProperty("viewDemand")).getText();
		System.out.println("getText" + getDemandText);
		String expectedDemand = Make + " " + Model;
		System.out.println("expectedDemand" + expectedDemand);
		Assert.assertEquals(getDemandText, expectedDemand);

	}

	// Dealer Tool Section Price Check link

	@Test(enabled=false,dataProvider = "DP_PriceCheck", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionviewdemand")
	public void DealerToolsectionpricecheck(String Location, String MfgeYear, String MfgMonth, String Make,
			String Model, String Variant, String Colour, String Kilometer, String Owner) throws InterruptedException {
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("priceChecklink"));
		clickOnElement(ToolSection_OR.getProperty("priceChecklink"));

		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		System.out.println("getFormTitle" + getText);
		Assert.assertEquals(getText, "CHECK IBB PRICE");

		explicitWait_object(ToolSection_OR.getProperty("btnSubmitprice"));
		clickOnElement(ToolSection_OR.getProperty("btnSubmitprice"));

		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualYearMdg")).isDisplayed(), true,
				"Validation message not display for Mang Year");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualLocationMsg")).isDisplayed(), true,
				"Validation message not display for Mang Location");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualKillometerMsg")).isDisplayed(), true,
				"Validation message not display for  Killometer");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualOwner")).isDisplayed(), true,
				"Validation message not display for Owner");
		// Select Location
		Thread.sleep(5000);
		explicitWait_object(ToolSection_OR.getProperty("locationPrice"));
		selectOption(ToolSection_OR.getProperty("locationPrice"), Location);
		// Select Year
		explicitWait_object(ToolSection_OR.getProperty("mfgYearPrice"));
		selectOption(ToolSection_OR.getProperty("mfgYearPrice"), MfgeYear);
		// Select Month
		explicitWait_object(ToolSection_OR.getProperty("mfgMonthPrice"));
		selectOption(ToolSection_OR.getProperty("mfgMonthPrice"), MfgMonth);
		// Select Make
		explicitWait_object(ToolSection_OR.getProperty("makePrice"));
		selectOption(ToolSection_OR.getProperty("makePrice"), Make);
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("modelPrice"));
		selectOption(ToolSection_OR.getProperty("modelPrice"), Model);
		// Select Variant
		explicitWait_object(ToolSection_OR.getProperty("variantPrice"));
		selectOption(ToolSection_OR.getProperty("variantPrice"), Variant);
		// Select Color
		explicitWait_object(ToolSection_OR.getProperty("colourPrice"));
		selectOption(ToolSection_OR.getProperty("colourPrice"), Colour);
		sendInput(ToolSection_OR.getProperty("kilometerPrice"), Kilometer);
		// Select Owner
		explicitWait_object(ToolSection_OR.getProperty("ownerPrice"));
		selectOption(ToolSection_OR.getProperty("ownerPrice"), Owner);
		clickOnElement(ToolSection_OR.getProperty("btnSubmitprice"));
		String actualColor = Colour.substring(0, 1).toLowerCase() + Colour.substring(1);
		System.out.println("actualColor" + actualColor);
		String getcompleteText = Make + " " + Model + " " + Variant + " " + "-" + " " + actualColor + " " + "-" + " "
				+ "January" + " " + MfgeYear + " " + "-" + " " + Location;

		// FIAT LINEA ACTIVE 1.3 - red - January 2016 - MUMBAI
		System.out.println("getcompleteText" + getcompleteText);
		String getPriceDetail = getObject(ToolSection_OR.getProperty("getPricedetailstext")).getText();
		System.out.println("getPriceDetail" + getPriceDetail);
		Assert.assertEquals(getPriceDetail, getcompleteText);
	}

	// Dealer Tool Section window sticker link

	@Test(enabled=false,dataProvider = "DP_WindowSticker", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionpricecheck")
	public void DealerToolsectionwindowsticker(String MfgeYear, String MfgMonth, String Fueltype, String Make,
			String Model, String Variant, String Transmission, String Colour, String Kilometer, String Owner,
			String Location, String RegiNo, String ExptPrice) {

		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("windowStickerlink"));
		clickOnElement(ToolSection_OR.getProperty("windowStickerlink"));

		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		System.out.println("getFormTitle" + getText);
		Assert.assertEquals(getText, "WINDOW STICKER");

		explicitWait_object(ToolSection_OR.getProperty("btnSubWindow"));
		clickOnElement(ToolSection_OR.getProperty("btnSubWindow"));

		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualYearMdg")).isDisplayed(), true,
				"Validation message not display for window Year");

		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowFuelMdg")).isDisplayed(), true,
				"Validation message not display for Fuel Type");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowTransmittionMsg")).isDisplayed(), true,
				"Validation message not display for Transmittion Type");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualKillometerMsg")).isDisplayed(), true,
				"Validation message not display for  Killometer");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualOwner")).isDisplayed(), true,
				"Validation message not display for Owner");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("residualLocationMsg")).isDisplayed(), true,
				"Validation message not display for window Location");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowRegNoMsg")).isDisplayed(), true,
				"Validation message not display for window Registration No");
		Assert.assertEquals(getObject(ToolSection_OR.getProperty("windowExpectedPrice")).isDisplayed(), true,
				"Validation message not display for window Expected Price");
		// Select Year
		explicitWait_object(ToolSection_OR.getProperty("mfgYearWindow"));
		selectOption(ToolSection_OR.getProperty("mfgYearWindow"), MfgeYear);
		// Select Month
		explicitWait_object(ToolSection_OR.getProperty("mfgMonthWindow"));
		selectOption(ToolSection_OR.getProperty("mfgMonthWindow"), MfgMonth);
		// Select Fuel Type
		explicitWait_object(ToolSection_OR.getProperty("fueltypeWindow"));
		selectOption(ToolSection_OR.getProperty("fueltypeWindow"), Fueltype);
		// Select Fuel Type
		explicitWait_object(ToolSection_OR.getProperty("makeWindow"));
		selectOption(ToolSection_OR.getProperty("makeWindow"), Make);
		// Select Model
		explicitWait_object(ToolSection_OR.getProperty("modelWindow"));
		selectOption(ToolSection_OR.getProperty("modelWindow"), Model);
		// Select Variant
		explicitWait_object(ToolSection_OR.getProperty("variantWindow"));
		selectOption(ToolSection_OR.getProperty("variantWindow"), Variant);
		// Select Transmission
		explicitWait_object(ToolSection_OR.getProperty("transmnWindow"));
		selectOption(ToolSection_OR.getProperty("transmnWindow"), Transmission);
		// Select Colour
		explicitWait_object(ToolSection_OR.getProperty("colourWindow"));
		selectOption(ToolSection_OR.getProperty("colourWindow"), Colour);

		sendInput(ToolSection_OR.getProperty("kilometerPrice"), Kilometer);
		// Select Owner
		explicitWait_object(ToolSection_OR.getProperty("ownerPrice"));
		selectOption(ToolSection_OR.getProperty("ownerPrice"), Owner);
		// Select Location
		explicitWait_object(ToolSection_OR.getProperty("locationWindow"));
		selectOption(ToolSection_OR.getProperty("locationWindow"), Location);
		sendInput(ToolSection_OR.getProperty("reginoWindow"), RegiNo);

		sendInput(ToolSection_OR.getProperty("exptprcWindow"), ExptPrice);
		clickOnElement(ToolSection_OR.getProperty("sellingprice"));
		String parentHandle = driver.getWindowHandle();
		clickOnElement(ToolSection_OR.getProperty("btnSubWindow"));
		windowHandling();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);

		}
		String getText1 = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/h3")).getText();
		System.out.println("getText1" + getText1);
		Assert.assertEquals(getText1, "test dealer");
		driver.close();
		driver.switchTo().window(parentHandle);

	}

	@Test(enabled=false,dataProvider = "DP_WindowSticker", dataProviderClass = DataProviderforDealerTools.class, dependsOnMethods = "DealerToolsectionpricecheck")
	public void dealerOffloadVehicles(String MfgeYear, String MfgMonth, String Fueltype, String Make, String Model,
			String Variant, String Transmission, String Colour, String Kilometer, String Owner, String Location,
			String RegiNo, String ExptPrice) {
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));

		explicitWait_object(ToolSection_OR.getProperty("subTabOffloadVehicle"));
		clickOnElement(ToolSection_OR.getProperty("subTabOffloadVehicle"));

		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		System.out.println("getFormTitle" + getText);
		Assert.assertEquals(getText, "OFFLOAD VEHICLES");
//		explicitWait_object(StockSection_OR.getProperty("btnAddOfLeadStock"));
//		clickOnElement(StockSection_OR.getProperty("btnAddOfLeadStock"));
	}

	//@Test(dataProvider = "DP_offLoadManageStock", dataProviderClass = DataProviderForManageStock.class,dependsOnMethods = "dealerOffloadVehicles")
	@Test(dataProvider = "DP_offLoadManageStock", dataProviderClass = DataProviderForManageStock.class)
	public void dealeroffLoadStockSection(String StockStatus, String StockCategory, String MfgYear, String MfgMonth,
			String Make, String Model, String Variant, String Colour, String Kilometer, String Owner, String RegNumber,
			String RegCity, String RegYear, String RegMonth, String Insurance, String InsuranceExpiry, String ChassisNo,
			String EngineNo, String CertifiedBy, String AboutStock, String SellingPrice, String DealerPrice)
					throws InterruptedException {
		dealerLogin();
		// Add New Stock
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));
		explicitWait_object(ToolSection_OR.getProperty("subTabOffloadVehicle"));
		clickOnElement(ToolSection_OR.getProperty("subTabOffloadVehicle"));
		String getText = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		System.out.println("getFormTitle" + getText);
		Assert.assertEquals(getText, "OFFLOAD VEHICLES");
		explicitWait_object(ToolSection_OR.getProperty("btnAddOfLeadStock"));
		clickOnElement(ToolSection_OR.getProperty("btnAddOfLeadStock"));
		windowHandling();
		dealerAddStock(StockStatus, StockCategory, MfgYear, MfgMonth, Make, Model, Variant, Colour, Kilometer, Owner,
				RegNumber, RegCity, RegYear, RegMonth, Insurance, InsuranceExpiry, ChassisNo, EngineNo, CertifiedBy,
				AboutStock, SellingPrice, DealerPrice);
		String getSuccessMsgText=getObject(StockSection_OR.getProperty("addStockMsg")).getText();
		System.out.println("getSuccessMsgText"+getSuccessMsgText);
		String expectedSuccessMsg=Make+" "+Model+""+Variant;
		System.out.println("expectedSuccessMsg"+expectedSuccessMsg);
		Thread.sleep(2000);
		driver.navigate().refresh();
	}
}
