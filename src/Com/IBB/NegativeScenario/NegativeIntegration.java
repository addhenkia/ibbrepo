package Com.IBB.NegativeScenario;

import org.testng.Assert;
import org.testng.annotations.Test;

import Com.IBB.PositiveScenario.DataproviderForIntegration;
import Com.IBB.TestBase.TestBase;

public class NegativeIntegration extends TestBase{

	// Scenario 2: Rate your Experience when user not logged In
		@Test(dataProvider = "DP_RateExpUserNotLogIn", dataProviderClass = DataproviderForIntegration.class)
		public void verifyRateYourExpUserNotLogIn(String Year, String Make, String Model, String Variant, String Killometer,
				String Feedback, String Msg) throws InterruptedException {
			driver.get(CONFIG.getProperty("testURL"));
	    	selectCity();
			explicitWait_object(HomePage_OR.getProperty("buttonImABuyer"));
			clickOnElement(HomePage_OR.getProperty("buttonImABuyer"));
			userType(Year, Make, Model, Variant, Killometer);
			rateExpUsernotlogIn(Feedback, Msg);
			driver.get(CONFIG.getProperty("testURL"));
			explicitWait_object(HomePage_OR.getProperty("buttonImABuyer"));
			clickOnElement(HomePage_OR.getProperty("buttonImABuyer"));
			userType(Year, Make, Model, Variant, Killometer);
			explicitWait_object(HomePage_OR.getProperty("btnsellyourCar"));
			clickOnElement(HomePage_OR.getProperty("btnsellyourCar"));
			popupLogin();
			websiteLogout();

		}
		
		// Scenario 3 : Rate your Experience
		@Test(dataProvider = "DP_RateExpUserNotLogIn", dataProviderClass = DataproviderForIntegration.class, dependsOnMethods = "verifyRateYourExpUserNotLogIn")
		public void verifyScenario3RateYourExpUserNotLogIn(String Year, String Make, String Model, String Variant, String Killometer,
				String Feedback, String Msg) throws InterruptedException {
			navigatetoUsecarValuation();
			clickOnElement(HomePage_OR.getProperty("btnBuyCar"));
			sellACarwithBuyerSeller(Year, Make, Model, Variant, Killometer);
			rateExpUsernotlogIn(Feedback, Msg);
			navigatetoUsecarValuation();
			clickOnElement(HomePage_OR.getProperty("btnBuyCar"));
			sellACarwithBuyerSeller(Year, Make, Model, Variant, Killometer);
			explicitWait_object(HomePage_OR.getProperty("btnsellyourCar"));
			clickOnElement(HomePage_OR.getProperty("btnsellyourCar"));
			popupLogin();
			websiteLogout();
			
		}
		//Create Lead when user is not Logged in
	/*   @Test(dataProvider="DP_ProductLead",dataProviderClass=DataProviderForCarListing.class,dependsOnMethods="verifyScenario3RateYourExpUserNotLogIn")
	    public void validateLeadGenUserNotLoggedIn(String LeadType,String LeadSource) throws InterruptedException
		{
	    	
	    	explicitWait_object(LeadGeneration_OR.getProperty("menuProduct"));
			clickOnElement(LeadGeneration_OR.getProperty("menuProduct"));
			
			explicitWait_object(LeadGeneration_OR.getProperty("subMenuInsurence"));
			clickOnElement(LeadGeneration_OR.getProperty("subMenuInsurence"));
			String parentHandle = driver.getWindowHandle(); 
			explicitWait_object(LeadGeneration_OR.getProperty("ImInterested"));
			clickOnElement(LeadGeneration_OR.getProperty("ImInterested"));
			popupLogin();
			Thread.sleep(2000);
			// get the current window handle
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); 
			}
			driver.close();
			driver.switchTo().window(parentHandle);
			driver.navigate().refresh();
			websiteLogout();
			callCenterCustomerDetails();
			productLead(LeadType,LeadSource);
		}*/
	  //Scenario 7 :Car Listing & Car detail page	: when user not logged in
	    @Test(dataProvider="DP_CarListingDetails",dataProviderClass=DataproviderForIntegration.class,dependsOnMethods="verifyScenario3RateYourExpUserNotLogIn")
	    public void validateCarForSale(String Make,String Model,String MinPrice,String MaxPrice,String LeadType,String LeadSource) throws InterruptedException
	    {
	    	//driver.get(CONFIG.getProperty("testURL"));
	    	carForSale(Make,Model,MinPrice,MaxPrice);
	    	windowHandling();
	    	explicitWait_object(CarForSale_OR.getProperty("username"));
	    	sendInput(CarForSale_OR.getProperty("username"), CONFIG.getProperty("websiteUser"));
	    	explicitWait_object(CarForSale_OR.getProperty("password"));
	    	sendInput(CarForSale_OR.getProperty("password"),CONFIG.getProperty("websitePassword"));
	    	clickOnElement(CarForSale_OR.getProperty("btnLogIn"));
	    	Assert.assertEquals(getObject(CarForSale_OR.getProperty("getSuccessMsg")).isDisplayed(),true,"Not success");
	        websiteLogout();
	        callcenterLogin();
	        productLead(LeadType,LeadSource);
	    }
	    //Scenario 8: Road Price
	    @Test(dataProvider = "DP_RoadPrice", dataProviderClass = DataproviderForIntegration.class,dependsOnMethods="validateCarForSale")
		public void verifyCarRoadPrice(String Make, String Model, String City) throws InterruptedException {
	    	driver.get(CONFIG.getProperty("testURL"));
			navigateOnRoadPrice(Make, Model, City);
			clickOnElement(RoadPrice_OR.getProperty("btnBookATestDrive"));
			// If user not logged in
			windowHandling();
			explicitWait_object(RoadPrice_OR.getProperty("userID"));
			clickOnElement(RoadPrice_OR.getProperty("userID"));
			Thread.sleep(1000);
			sendInput(RoadPrice_OR.getProperty("userID"), CONFIG.getProperty("websiteUser"));
			sendInput(RoadPrice_OR.getProperty("password"), CONFIG.getProperty("websitePassword"));
			clickOnElement(RoadPrice_OR.getProperty("btnLogin"));
			explicitWait_object(RoadPrice_OR.getProperty("welcomeUsername"));
			Assert.assertEquals(getObject(RoadPrice_OR.getProperty("welcomeUsername")).isDisplayed(), true,
					"Login not successfull");
			Assert.assertEquals(getObject(RoadPrice_OR.getProperty("thankumessage")).isDisplayed(), true,
					"Thank you message is  not  display");
			// Application log out
			websiteLogout();
		}
}
