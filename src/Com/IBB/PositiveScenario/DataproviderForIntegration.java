package Com.IBB.PositiveScenario;

import org.testng.annotations.DataProvider;

import Com.IBB.TestBase.TestBase;

public class DataproviderForIntegration extends TestBase{
	 //I am a seller
		@DataProvider(name = "DP_IMASeller")
		public static Object[][] seller() throws Exception
		{
			Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"HomePage","IMASeller");
			return retObjArr;
		}

		//I am a seller- Check Again 
			@DataProvider(name = "DP_CheckAgain")
			public static Object[][] CheckAgain () throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"HomePage","CheckAgain");
				return retObjArr;
			}
			 //I am a buyer
			@DataProvider(name = "DP_IMABuyer")
			public static Object[][] buyer() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"HomePage","IMABuyer");
				return retObjArr;
			}
			//On Road Price
			@DataProvider(name = "DP_Rate")
			public static Object[][] roadPrice() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"HomePage","RateExp");
				return retObjArr;
			}
			@DataProvider(name = "DP_OtherLead")
			public static Object[][] otherLead() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"CarListing","OtherLead");
				return retObjArr;
			}
			 //Car valuation sell
			@DataProvider(name = "DP_CarValuationSell")
			public static Object[][] carValuation() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"HomePage","CarValuationSell");
				return retObjArr;
			}
			//Car valuation buy
			@DataProvider(name = "DP_CarValuationBuy")
			public static Object[][] carValuationbuy() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"HomePage","CarValuationBuy");
				return retObjArr;
			}
			@DataProvider(name = "DP_ProductLead")
			public static Object[][] productLead() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"CarListing","ProductLead");
				return retObjArr;
			}
			@DataProvider(name = "DP_ResidualDetails")
			public static Object[][] residualDetails() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"Residual","ResidualDetails");
				return retObjArr;
			}

			@DataProvider(name = "DP_CarListingDetails")
			public static Object[][] carListing() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"CarListing","CarListingDetails");
				return retObjArr;
			}
			
			// On Road Price
			@DataProvider(name = "DP_RoadPrice")
			public static Object[][] roadPrice1() throws Exception {
				Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "HomePage", "RoadPrice");
				return retObjArr;
			}
			// Check Exchange Price
			@DataProvider(name = "DP_ExchangePrice")
			public static Object[][] ExchangePrice() throws Exception {
				Object[][] retObjArr = getTableArray(CONFIG.getProperty("DataProviderPath"), "HomePage", "ExchangePrice");
				return retObjArr;
			}
			//when user not log in
			@DataProvider(name = "DP_RateExpUserNotLogIn")
			public static Object[][] rateExpUSerNotLogIn() throws Exception
			{
				Object[][] retObjArr=getTableArray(CONFIG.getProperty("DataProviderPath"),"HomePage","RateExpUserNotLogIn");
				return retObjArr;
			}
}
