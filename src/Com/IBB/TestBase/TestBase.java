package Com.IBB.TestBase;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;

import org.testng.annotations.BeforeSuite;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class TestBase extends UniqueID {

	public static Logger APP_LOGS = null;
	public static Properties CONFIG = null;
	public static Properties HomePage_OR = null;
	public static Properties RoadPrice_OR = null;
	public static Properties CarForSale_OR = null;
	public static Properties Residual_OR = null;
	public static Properties LeadGeneration_OR = null;
	public static Properties CallCenter_OR = null;
	public static Properties StockSection_OR = null;
	public static Properties ToolSection_OR = null;
	public static boolean isInitialized = false;
	public static boolean isBrowserOpened = false;
	public static Hashtable<String, String> sessionData = new Hashtable<String, String>();
	public static WebDriver driver = null;
	public static Actions act = null;
	public static List<WebElement> menu = null;
	public static List<WebElement> menu1 = null;
	public static List<WebElement> flop = null;
	public static String message = null;
	protected String uniqueID = (new UniqueTestId()).id;

	@BeforeSuite
	public void beforeTest() throws Exception {
		initialize();
		openBrowser();
		driver.get(CONFIG.getProperty("testURL"));
		driver.manage().window().maximize();
	}

	@AfterSuite
	public void afterClass() throws Exception {

		closeBrowser();
	}

	// initializing the Tests
	public void initialize() throws Exception {
		// logs
		if (!isInitialized) {
			APP_LOGS = Logger.getLogger("devpinoyLogger");

			APP_LOGS.debug("Loading  CONFIG Properties file");
			CONFIG = new Properties();
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//CONFIG.properties");
			CONFIG.load(ip);
			APP_LOGS.debug("CONFIG Properties file loaded successfully");

			APP_LOGS.debug("Loading  HomePage_OR Properties file");
			HomePage_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//HomePage_OR.properties");
			HomePage_OR.load(ip);
			APP_LOGS.debug("HomePage OR Properties file loaded successfully");

			APP_LOGS.debug("Loading  RoadPrice_OR Properties file");
			RoadPrice_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//RoadPrice_OR.properties");
			RoadPrice_OR.load(ip);
			APP_LOGS.debug("RoadPrice OR Properties file loaded successfully");

			APP_LOGS.debug("Loading  CarForSale_OR Properties file");
			CarForSale_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//CarForSale_OR.properties");
			CarForSale_OR.load(ip);
			APP_LOGS.debug("CarForSale OR Properties file loaded successfully");

			APP_LOGS.debug("Loading  Residual_OR Properties file");
			Residual_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//Residual_OR.properties");
			Residual_OR.load(ip);
			APP_LOGS.debug("Residual OR Properties file loaded successfully");

			APP_LOGS.debug("Loading  LeadGeneration_OR Properties file");
			LeadGeneration_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//LeadGeneration_OR.properties");
			LeadGeneration_OR.load(ip);
			APP_LOGS.debug("LeadGeneration OR Properties file loaded successfully");

			APP_LOGS.debug("Loading  CallCenter_OR Properties file");
			CallCenter_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//CallCenter_OR.properties");
			CallCenter_OR.load(ip);
			APP_LOGS.debug("CallCenter_OR Properties file loaded successfully");

			APP_LOGS.debug("Loading  StockSection_OR Properties file");
			StockSection_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//StockSection_OR.properties");
			StockSection_OR.load(ip);
			APP_LOGS.debug("StockSection_OR Properties file loaded successfully");

			APP_LOGS.debug("Loading  ToolSection_OR Properties file");
			ToolSection_OR = new Properties();
			ip = new FileInputStream(
					System.getProperty("user.dir") + "//src//Com//IBB//Config//ToolSection_OR.properties");
			ToolSection_OR.load(ip);
			APP_LOGS.debug("ToolSection_OR Properties file loaded successfully");

			isInitialized = true;
		}
	}

	// To open browser
	public void openBrowser() {
		if (!isBrowserOpened) {
			if (CONFIG.getProperty("browser").equals("Firefox"))
				try {
					System.setProperty("webdriver.firefox.bin",
							"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
					driver = new FirefoxDriver();
				} catch (Exception e) {
					// TO DO Auto-generated catch block
					e.printStackTrace();
				}

			isBrowserOpened = true;
			String waitTime = CONFIG.getProperty("default_implicitWait");
			driver.manage().timeouts().implicitlyWait(Long.parseLong(waitTime), TimeUnit.SECONDS);
		}
		APP_LOGS.debug("Browser launched successfully");
	}

	// close browser
	public void closeBrowser() {

		driver.quit();
		isBrowserOpened = false;
		APP_LOGS.debug("Browser closed successfully");

	}

	// Reading x-path from properties file
	public static WebElement getObject(String xpathKeyWithORName) {

		try {
			WebElement x = driver.findElement(By.xpath(xpathKeyWithORName));
			return x;
		} catch (Throwable t) {
			// report error
			APP_LOGS.debug("Cannot find object with key -- " + xpathKeyWithORName);
			return null;
		}
	}

	// To sent values to text-box
	public void sendInput(String xpathKeyWithORName, String Input) {
		getObject(xpathKeyWithORName).clear();
		getObject(xpathKeyWithORName).sendKeys(Input);
		APP_LOGS.debug("Sent -- " + Input + " in webelement with XpathKey -- " + xpathKeyWithORName);
	}

	// To perform click action
	public void clickOnElement(String xpathKeyWithORName) {
		try {
			getObject(xpathKeyWithORName).click();
			APP_LOGS.debug("Clicked on webelement with XpathKey -- " + xpathKeyWithORName);
		} catch (Exception e) {
			APP_LOGS.debug("Element with XpathKey -- " + xpathKeyWithORName + " is not clickable");
		}
	}

	// Explicitly wait until expected condition
	public void explicitWait_object(String xpathKeyWithORName) {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			wait.until(ExpectedConditions.titleContains(xpathKeyWithORName));
		} catch (Exception e) {
			APP_LOGS.debug(xpathKeyWithORName + " -- page not loaded within timeout");
		}
	}

	/*
	 * public void explicitWait_object(String xpathKeyWithORName) {
	 * WebDriverWait wait = new WebDriverWait(driver, 10);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "xpathKeyWithORName"))); }
	 */
	// For Select option from drop down by using text

	public void selectOption(String xpathKeyWithORName, String value) {
		WebElement dropDown = driver.findElement(By.xpath(xpathKeyWithORName));
		Select selectValue = new Select(dropDown);
		selectValue.selectByVisibleText(value);
	}

	// For Move to Master menu
	public void moveToMenu(String xpathKeyWithORName) {
		act = new Actions(driver);
		act.moveToElement(getObject(xpathKeyWithORName)).build().perform();
		APP_LOGS.debug("moved to the element with XpathKey" + xpathKeyWithORName);
	}

	// For Window/pop up Handling

	public void windowHandling() {
		String myWindowHandle = driver.getWindowHandle();
		driver.switchTo().window(myWindowHandle);
	}

	// For auto-serach Handling

	// To sent values to text-box
	public void autoSearch(String xpathKeyWithORName, String xpathKeyWithORName1, String Input) {
		getObject(xpathKeyWithORName).click();
		getObject(xpathKeyWithORName1).sendKeys(Input);
		getObject(xpathKeyWithORName1).sendKeys(Keys.ENTER);
		APP_LOGS.debug("Sent -- " + Input + " in webelement with XpathKey -- " + xpathKeyWithORName);
	}

	// IBB Web Site Log in
	public void websiteLogin() {
		explicitWait_object(RoadPrice_OR.getProperty("btnMyAccount"));
		clickOnElement(RoadPrice_OR.getProperty("btnMyAccount"));
		sendInput(RoadPrice_OR.getProperty("ibbUser"), CONFIG.getProperty("websiteUser"));
		sendInput(RoadPrice_OR.getProperty("ibbPassword"), CONFIG.getProperty("websitePassword"));
		clickOnElement(RoadPrice_OR.getProperty("btnLogIn"));
		//Thread.sleep(2000);
	}

	// IBB Web site Log out
	public void websiteLogout() throws InterruptedException {
		explicitWait_object(RoadPrice_OR.getProperty("btnMyAccount"));
		clickOnElement(RoadPrice_OR.getProperty("btnMyAccount"));
		explicitWait_object(RoadPrice_OR.getProperty("btnLogout"));
		clickOnElement(RoadPrice_OR.getProperty("btnLogout"));
		Thread.sleep(5000);
		// String getText =
		// getObject(RoadPrice_OR.getProperty("btnMyAccount")).getText();
		// Assert.assertEquals("MY ACCOUNT", getText);
	}

	// Call Center Login
	public void callcenterLogin() throws InterruptedException {
		driver.get(CONFIG.getProperty("callCenterURL"));
		driver.manage().timeouts().implicitlyWait((3000), TimeUnit.SECONDS);
		sendInput(RoadPrice_OR.getProperty("emailAddress"), CONFIG.getProperty("callcenterUname"));
		sendInput(RoadPrice_OR.getProperty("Cpassword"), CONFIG.getProperty("callcenterpassword"));
		clickOnElement(RoadPrice_OR.getProperty("btnCallCenterLogin"));
		Thread.sleep(2000);
		// String getText =
		// getObject(RoadPrice_OR.getProperty("callCenterUser")).getText();
		// Assert.assertEquals(getText, "IBB Admin");
	}

	// Call center Log out
	public void callcenterLogout() throws InterruptedException {
		clickOnElement(RoadPrice_OR.getProperty("custAccountTab"));
		clickOnElement(RoadPrice_OR.getProperty("btnCustLogout"));
		Thread.sleep(5000);
	}

	// Select city
	public void selectCity() {
		windowHandling();
		explicitWait_object(HomePage_OR.getProperty("selectCityDropdown"));
		clickOnElement(HomePage_OR.getProperty("selectCityDropdown"));
		WebElement cityList = getObject(HomePage_OR.getProperty("enterCity"));
		cityList.sendKeys("MUM");
		cityList.sendKeys(Keys.ARROW_DOWN);
		cityList.sendKeys(Keys.ENTER);
	}

	// @Author : Pritesh :To check values Again
	public void SellerBuyerCheckAgain(String Make, String Model, String Variant, String Year, String Killometer)
			throws InterruptedException {

		// Select Make
		explicitWait_object(HomePage_OR.getProperty("checkAgainMake"));
		clickOnElement(HomePage_OR.getProperty("checkAgainMake"));
		WebElement makeList = getObject(HomePage_OR.getProperty("enterCheckagainMake"));
		makeList.sendKeys(Make);
		makeList.sendKeys(Keys.ENTER);
		// Select Model
		explicitWait_object(HomePage_OR.getProperty("checkAgainModel"));
		clickOnElement(HomePage_OR.getProperty("checkAgainModel"));
		WebElement modelList = getObject(HomePage_OR.getProperty("enterCheckagainModel"));
		modelList.sendKeys(Model);
		modelList.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		// Select Variant
		explicitWait_object(HomePage_OR.getProperty("checkAgainVarient"));
		clickOnElement(HomePage_OR.getProperty("checkAgainVarient"));
		WebElement varientList = getObject(HomePage_OR.getProperty("enterCheckagainVarient"));
		varientList.sendKeys(Variant);
		varientList.sendKeys(Keys.ENTER);
		// Select Year
		explicitWait_object(HomePage_OR.getProperty("checkAgainYear"));
		clickOnElement(HomePage_OR.getProperty("checkAgainYear"));
		WebElement yearList = getObject(HomePage_OR.getProperty("enterCheckagainYear"));
		yearList.sendKeys(Year);
		yearList.sendKeys(Keys.ENTER);

		sendInput(HomePage_OR.getProperty("checkAgainKm"), Killometer);

	}

	// To Validate user Type(Buyer / Seller) details
	public void userType(String Year, String Make, String Model, String Variant, String Killometer) {
		// Select Year
		explicitWait_object(HomePage_OR.getProperty("selectYearDropdown"));
		clickOnElement(HomePage_OR.getProperty("selectYearDropdown"));
		WebElement yearList = getObject(HomePage_OR.getProperty("enterYear"));
		yearList.sendKeys(Year);
		yearList.sendKeys(Keys.ENTER);
		// Select Make
		explicitWait_object(HomePage_OR.getProperty("selectMakeDropdown"));
		clickOnElement(HomePage_OR.getProperty("selectMakeDropdown"));
		WebElement makeList = getObject(HomePage_OR.getProperty("enterMake"));
		makeList.sendKeys(Make);
		makeList.sendKeys(Keys.ENTER);

		// Select Model
		explicitWait_object(HomePage_OR.getProperty("selectModelDropdown"));
		clickOnElement(HomePage_OR.getProperty("selectModelDropdown"));
		WebElement modelList = getObject(HomePage_OR.getProperty("enterModel"));
		modelList.sendKeys(Model);
		modelList.sendKeys(Keys.ENTER);

		// Select Variant
		explicitWait_object(HomePage_OR.getProperty("selectVarient"));
		clickOnElement(HomePage_OR.getProperty("selectVarient"));
		WebElement varientList = getObject(HomePage_OR.getProperty("enterVarient"));
		varientList.sendKeys(Variant);
		varientList.sendKeys(Keys.ENTER);

		sendInput(HomePage_OR.getProperty("killometer"), Killometer);
		clickOnElement(HomePage_OR.getProperty("btnCheckNow"));
	}

	// Comman function for navigation to On road Price
	public void navigateOnRoadPrice(String Make, String Model, String City) throws InterruptedException {

		Thread.sleep(1000);
		explicitWait_object(RoadPrice_OR.getProperty("menuCheckCarPrice"));
		clickOnElement(RoadPrice_OR.getProperty("menuCheckCarPrice"));
		explicitWait_object(RoadPrice_OR.getProperty("submenuOnRoadPrice"));
		clickOnElement(RoadPrice_OR.getProperty("submenuOnRoadPrice"));
		// Select Make
		Thread.sleep(2000);
		explicitWait_object(RoadPrice_OR.getProperty("selectMake"));
		clickOnElement(RoadPrice_OR.getProperty("selectMake"));
		WebElement makeList = getObject(RoadPrice_OR.getProperty("enterMake"));
		makeList.sendKeys(Make);
		makeList.sendKeys(Keys.ENTER);
		// Select Model
		explicitWait_object(RoadPrice_OR.getProperty("selectModel"));
		clickOnElement(RoadPrice_OR.getProperty("selectModel"));
		WebElement modelList = getObject(RoadPrice_OR.getProperty("enterModel"));
		modelList.sendKeys(Model);
		modelList.sendKeys(Keys.ENTER);
		// Select City
		explicitWait_object(RoadPrice_OR.getProperty("selectCity"));
		clickOnElement(RoadPrice_OR.getProperty("selectCity"));
		WebElement ClList = getObject(RoadPrice_OR.getProperty("enterCity"));
		ClList.sendKeys(City);
		ClList.sendKeys(Keys.ENTER);
		explicitWait_object(RoadPrice_OR.getProperty("btnCheckNow"));
		clickOnElement(RoadPrice_OR.getProperty("btnCheckNow"));
	}

	// Navigation to Use car valuation
	public void navigatetoUsecarValuation() {
		explicitWait_object(RoadPrice_OR.getProperty("menuCheckCarPrice"));
		clickOnElement(RoadPrice_OR.getProperty("menuCheckCarPrice"));
		explicitWait_object(HomePage_OR.getProperty("menuCarValuation"));
		clickOnElement(HomePage_OR.getProperty("menuCarValuation"));
	}

	// Rate experience when user not log in
	public void rateExpUsernotlogIn(String Feedback, String Msg) throws InterruptedException {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,500)", "");
		clickOnElement(RoadPrice_OR.getProperty("rateExpSubmit"));
		windowHandling();
		explicitWait_object(RoadPrice_OR.getProperty("rateMandetarymsg"));
		String getText = getObject(RoadPrice_OR.getProperty("rateMandetarymsg")).getText();
		Assert.assertEquals(getText, "Please rate your experience by clicking on the stars.");
		clickOnElement(RoadPrice_OR.getProperty("btnRatePopupClose"));

		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scroll(0, -250);");
		explicitWait_object(RoadPrice_OR.getProperty("starMarks"));
		WebElement element = getObject(RoadPrice_OR.getProperty("starMarks"));
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].style.display = 'block'; arguments[0].style.visibility = 'visible';", element);
		element.click();
		clickOnElement(RoadPrice_OR.getProperty("rateemailID"));
		sendInput(RoadPrice_OR.getProperty("rateemailID"), CONFIG.getProperty("websiteUser"));
		sendInput(RoadPrice_OR.getProperty("rateMobNo"), CONFIG.getProperty("websiteMobNo"));
		sendInput(RoadPrice_OR.getProperty("rateShareFeedback"), Feedback);
		clickOnElement(RoadPrice_OR.getProperty("rateExpSubmit"));
		windowHandling();
		explicitWait_object(RoadPrice_OR.getProperty("rateMandetarymsg"));
		String getValidationMsg = getObject(RoadPrice_OR.getProperty("rateMandetarymsg")).getText();
		
		Assert.assertEquals(getValidationMsg, Msg);
		explicitWait_object(RoadPrice_OR.getProperty("btnPopupclose"));
		clickOnElement(RoadPrice_OR.getProperty("btnPopupclose"));
	}

	// Rate experience when user logged in
	public void rateExpUserLoggedIn(String Feedback, String Msg) throws InterruptedException {
		
		explicitWait_object(RoadPrice_OR.getProperty("starMarks"));
		WebElement element = getObject(RoadPrice_OR.getProperty("starMarks"));
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].style.display = 'block'; arguments[0].style.visibility = 'visible';", element);
		element.click();
		explicitWait_object(RoadPrice_OR.getProperty("rateemailID"));

		explicitWait_object(RoadPrice_OR.getProperty("rateMobNo"));

		sendInput(RoadPrice_OR.getProperty("rateShareFeedback"), Feedback);
		clickOnElement(RoadPrice_OR.getProperty("rateExpSubmit"));
		windowHandling();
		Thread.sleep(1000);
		explicitWait_object(RoadPrice_OR.getProperty("rateMandetarymsg"));
		String getValidationMsg = getObject(RoadPrice_OR.getProperty("rateMandetarymsg")).getText();
		Assert.assertEquals(getValidationMsg, Msg);
		explicitWait_object(RoadPrice_OR.getProperty("btnPopupclose"));
		clickOnElement(RoadPrice_OR.getProperty("btnPopupclose"));
	}

	// Scenario 7: Sale a Car
	public void carForSale(String Make, String Model, String MinPrice, String MaxPrice) throws InterruptedException {
		explicitWait_object(CarForSale_OR.getProperty("menuCarForSale"));
		clickOnElement(CarForSale_OR.getProperty("menuCarForSale"));
		explicitWait_object(CarForSale_OR.getProperty("subMenuExplorNewCars"));
		clickOnElement(CarForSale_OR.getProperty("subMenuExplorNewCars"));
		explicitWait_object(CarForSale_OR.getProperty("selectMake"));
		clickOnElement(CarForSale_OR.getProperty("selectMake"));
		WebElement makeList = getObject(CarForSale_OR.getProperty("enterMake"));
		makeList.sendKeys(Make);
		makeList.sendKeys(Keys.ENTER);
		String getMake = getObject(CarForSale_OR.getProperty("selectMake")).getText();
		// Select Make
		explicitWait_object(CarForSale_OR.getProperty("selectModel"));
		clickOnElement(CarForSale_OR.getProperty("selectModel"));
		WebElement modelList = getObject(CarForSale_OR.getProperty("enterModel"));
		modelList.sendKeys(Model);
		modelList.sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		String getTextmodel = getObject(CarForSale_OR.getProperty("selectModel")).getText();
		sendInput(CarForSale_OR.getProperty("minPrice"), MinPrice);
		Thread.sleep(1000);
		sendInput(CarForSale_OR.getProperty("maxPrice"), MaxPrice);
		Thread.sleep(1000);
		clickOnElement(CarForSale_OR.getProperty("btnNext"));

		String getdisplayingMake = getObject(CarForSale_OR.getProperty("displayingMake")).getAttribute("value");
		String getdisplayingModel = getObject(CarForSale_OR.getProperty("displayingModel")).getAttribute("value");
		String getdisplayingMinmax = getObject(CarForSale_OR.getProperty("displayingMinMax")).getAttribute("value");
		String getMinMax = MinPrice + "-" + MaxPrice;
		
		  Assert.assertEquals(getMake, getdisplayingMake);
		  Assert.assertEquals(getTextmodel, getdisplayingModel);
		 Assert.assertEquals(getMinMax, getdisplayingMinmax);
		 
		explicitWait_object(CarForSale_OR.getProperty("viewDetails"));
		clickOnElement(CarForSale_OR.getProperty("viewDetails"));

		clickOnElement(CarForSale_OR.getProperty("btnTakeATestDrive"));
	}

	// Scenario7: validate created Lead at call center
	public void productLead(String LeadType, String LeadSource) throws InterruptedException {
		// For current date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);
		// callcenterLogin();
		clickOnElement(RoadPrice_OR.getProperty("productLeadTab"));
		explicitWait_object(CarForSale_OR.getProperty("userMobNo"));
		sendInput(CarForSale_OR.getProperty("userMobNo"), CONFIG.getProperty("websiteMobNo"));
		Thread.sleep(2000);
		explicitWait_object(CarForSale_OR.getProperty("leadDate"));
		String actualDate = getObject(CarForSale_OR.getProperty("leadDate")).getText();
		Assert.assertEquals(actualDate, systemDate);
		explicitWait_object(CarForSale_OR.getProperty("leadType"));
		String actualLeadType = getObject(CarForSale_OR.getProperty("leadType")).getText();
		Assert.assertEquals(actualLeadType, LeadType);
		explicitWait_object(CarForSale_OR.getProperty("leadSource"));
		String actualleadSource = getObject(CarForSale_OR.getProperty("leadSource")).getText();
		Assert.assertEquals(actualleadSource, LeadSource);
		callcenterLogout();
		Thread.sleep(5000);
	}

	//
	// Scenario1: validate created procurement Lead at call center
	public void procurementLead() throws InterruptedException {
		// Call Center Login : Product Lead Tab
		explicitWait_object(RoadPrice_OR.getProperty("procurmentLeadTab"));
		clickOnElement(RoadPrice_OR.getProperty("procurmentLeadTab"));
		explicitWait_object(RoadPrice_OR.getProperty("procurementEmail"));
		sendInput(RoadPrice_OR.getProperty("procurementEmail"), CONFIG.getProperty("websiteUser"));
		explicitWait_object(RoadPrice_OR.getProperty("procurementDate"));
		// For current date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);
		String getProcurmentDate = getObject(RoadPrice_OR.getProperty("procurementDate")).getText();
		Assert.assertEquals(getProcurmentDate, systemDate);

		String ProcurmentLeadGetsource = getObject(RoadPrice_OR.getProperty("procurementSource")).getText();
		Assert.assertEquals(ProcurmentLeadGetsource, "Ibbweb");

		String ProcurmentGetEmail = getObject(RoadPrice_OR.getProperty("procurementGetEmail")).getText();
		Assert.assertEquals(ProcurmentGetEmail, CONFIG.getProperty("websiteUser"));

		String ProcurmentGetMob = getObject(RoadPrice_OR.getProperty("procurementMobNo")).getText();
		Assert.assertEquals(ProcurmentGetMob, CONFIG.getProperty("websiteMobNo"));

	}

	// Call Center : Customer Details Verification
	public void callCenterCustomerDetails() throws InterruptedException {
		callcenterLogin();
		// Call center customer tab details verification
		clickOnElement(RoadPrice_OR.getProperty("customerTab"));
		explicitWait_object(RoadPrice_OR.getProperty("custEmail"));
		sendInput(RoadPrice_OR.getProperty("custEmail"), CONFIG.getProperty("websiteUser"));
		Thread.sleep(5000);
		explicitWait_object(RoadPrice_OR.getProperty("custEmail1"));
		String getEmail = getObject(RoadPrice_OR.getProperty("custEmail1")).getText();
		Assert.assertEquals(CONFIG.getProperty("websiteUser"), getEmail);
		explicitWait_object(RoadPrice_OR.getProperty("custGender"));
		String getGender = getObject(RoadPrice_OR.getProperty("custGender")).getText();
		Assert.assertEquals(getGender, "Male");
		explicitWait_object(RoadPrice_OR.getProperty("newsLetter"));
		String getNewsLetter = getObject(RoadPrice_OR.getProperty("newsLetter")).getText();
		Assert.assertEquals(getNewsLetter, "Subscribed");

	}

	// Call Center : Add Lead: Verify Procurement lead
	public void procurementLeadCallCenter(String LeadType, String Name, String Phoneno, String Email, String State,
			String City, String Make, String Model, String Variant, String Colour, String Kilometer, String Owner,
			String RegNumber) throws InterruptedException {
		clickOnElement(RoadPrice_OR.getProperty("procurmentLeadTab"));
		sendInput(RoadPrice_OR.getProperty("procurementEmail"), Email);

		Thread.sleep(2000);
		// For current date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);
		String ProcurmentLeadGetsource = getObject(RoadPrice_OR.getProperty("procurementSource")).getText();
		Assert.assertEquals(ProcurmentLeadGetsource, "Call Center");
		String getProcurmentDate = getObject(RoadPrice_OR.getProperty("procurementDate")).getText();
		Assert.assertEquals(getProcurmentDate, systemDate);
		String procurementGetCustMob = getObject(CallCenter_OR.getProperty("procurementGetCustMob")).getText();
		Assert.assertEquals(procurementGetCustMob, Phoneno);
		String procurementGetEmail = getObject(CallCenter_OR.getProperty("procurementGetEmail")).getText();
		Assert.assertEquals(procurementGetEmail, Email);
		JavascriptExecutor je = (JavascriptExecutor) driver;

		WebElement element = getObject(CallCenter_OR.getProperty("procurementGetMake"));

		je.executeScript("arguments[0].scrollIntoView(true);", element);
		String procurementGetMake = getObject(CallCenter_OR.getProperty("procurementGetMake")).getText();
		Assert.assertEquals(procurementGetMake, Make);
		String procurementGetModel = getObject(CallCenter_OR.getProperty("procurementGetModel")).getText();
		Assert.assertEquals(procurementGetModel, Model);
		String procurementGetVariant = getObject(CallCenter_OR.getProperty("procurementGetVariant")).getText();
		Assert.assertEquals(procurementGetVariant, Variant);
		// String procurementGetKms =
		// getObject(CallCenter_OR.getProperty("procurementGetKms")).getText();
		// Assert.assertEquals(procurementGetKms, Kilometer);
		String procurementGetOwner = getObject(CallCenter_OR.getProperty("procurementGetOwner")).getText();
		Assert.assertEquals(procurementGetOwner, Owner);
		String procurementGetRegNo = getObject(CallCenter_OR.getProperty("procurementGetRegNo")).getText();
		Assert.assertEquals(procurementGetRegNo, RegNumber + uniqueID);
	}

	// Call Center : Add Lead: Verify Product lead
	public void productLeadCallCenter(String LeadTags, String LeadType, String Name, String Phoneno, String Email,
			String State, String City, String Make, String Model, String Variant, String Colour, String Kilometer)
					throws InterruptedException {

		clickOnElement(RoadPrice_OR.getProperty("productLeadTab"));
		explicitWait_object(CarForSale_OR.getProperty("userMobNo"));
		sendInput(CarForSale_OR.getProperty("userMobNo"), Phoneno + uniqueID);
		Thread.sleep(2000);
		explicitWait_object(CallCenter_OR.getProperty("productLeadType"));
		String productLeadType = getObject(CallCenter_OR.getProperty("productLeadType")).getText();
		Assert.assertEquals(productLeadType, LeadType);

		String productLeadSource = getObject(CallCenter_OR.getProperty("productLeadSource")).getText();
		Assert.assertEquals(productLeadSource, "Call Center");

		// String productLeadTags =
		// getObject(CallCenter_OR.getProperty("productLeadTags")).getText();
		// Assert.assertEquals(productLeadTags, LeadTags);
		// For current date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);
		explicitWait_object(CarForSale_OR.getProperty("productLeadDate"));
		String actualDate = getObject(CallCenter_OR.getProperty("productLeadDate")).getText();
		Assert.assertEquals(actualDate, systemDate);

		String productLeadName = getObject(CallCenter_OR.getProperty("productLeadName")).getText();
		Assert.assertEquals(productLeadName, Name);

		String productLeadMob = getObject(CallCenter_OR.getProperty("productLeadMob")).getText();
		Assert.assertEquals(productLeadMob, Phoneno + uniqueID);

		String productLeadState = getObject(CallCenter_OR.getProperty("productLeadState")).getText();
		Assert.assertEquals(productLeadState, State);

		// String productLeadSCity =
		// getObject(CallCenter_OR.getProperty("productLeadSCity")).getText();
		// Assert.assertEquals(productLeadSCity, City);
		/*
		 * JavascriptExecutor je = (JavascriptExecutor) driver;
		 * 
		 * WebElement element =
		 * getObject(CallCenter_OR.getProperty("productLeadSMake"));
		 * 
		 * je.executeScript("arguments[0].scrollIntoView(true);", element);
		 * String productLeadSMake =
		 * getObject(CallCenter_OR.getProperty("productLeadSMake")).getText();
		 * Assert.assertEquals(productLeadSMake, Make);
		 * 
		 * String productLeadSModel =
		 * getObject(CallCenter_OR.getProperty("productLeadSModel")).getText();
		 * Assert.assertEquals(productLeadSModel, Model);
		 * 
		 * String productLeadSVariant =
		 * getObject(CallCenter_OR.getProperty("productLeadSVariant")).getText()
		 * ; Assert.assertEquals(productLeadSVariant, Variant);
		 * 
		 * String productLeadSColor =
		 * getObject(CallCenter_OR.getProperty("productLeadSColor")).getText();
		 * Assert.assertEquals(productLeadSColor, Colour);
		 */

	}

	// Call Center : Add Lead: Verify other lead
	public void otherLeadCallCenter(String LeadType, String Name, String Phoneno, String Email, String State,
			String City, String Make, String Model, String Variant, String Colour, String Kilometer)
					throws InterruptedException {

		explicitWait_object(RoadPrice_OR.getProperty("otherLeadTab"));
		clickOnElement(RoadPrice_OR.getProperty("otherLeadTab"));
		explicitWait_object(CallCenter_OR.getProperty("otherLeadMobNo"));
		sendInput(CallCenter_OR.getProperty("otherLeadMobNo"), Phoneno + uniqueID);
		Thread.sleep(2000);
		if (LeadType.equalsIgnoreCase("RTO")) {
			String actualotherLeadType = getObject(CallCenter_OR.getProperty("otherLeadType")).getText();
			Assert.assertEquals(actualotherLeadType.toUpperCase(), LeadType);
		} else {
			String actualotherLeadType = getObject(CallCenter_OR.getProperty("otherLeadType")).getText();
			Assert.assertEquals(actualotherLeadType, LeadType);
		}
		// For current date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);
		explicitWait_object(CarForSale_OR.getProperty("productLeadDate"));
		String actualDate = getObject(CallCenter_OR.getProperty("otherLeadDate")).getText();
		Assert.assertEquals(actualDate, systemDate);

		String otherLeadName = getObject(CallCenter_OR.getProperty("otherLeadName")).getText();
		Assert.assertEquals(otherLeadName, Name);

		String otherLeadMob = getObject(CallCenter_OR.getProperty("otherLeadMob")).getText();
		Assert.assertEquals(otherLeadMob, Phoneno + uniqueID);

		String otherLeadState = getObject(CallCenter_OR.getProperty("otherLeadState")).getText();
		Assert.assertEquals(otherLeadState, State);

		// String otherLeadSCity =
		// getObject(CallCenter_OR.getProperty("otherLeadSCity")).getText();
		// Assert.assertEquals(otherLeadSCity, City);
		/*
		 * JavascriptExecutor je = (JavascriptExecutor) driver;
		 * 
		 * WebElement element =
		 * getObject(CallCenter_OR.getProperty("otherLeadSMake"));
		 * 
		 * je.executeScript("arguments[0].scrollIntoView(true);", element);
		 * 
		 * String otherLeadSMake =
		 * getObject(CallCenter_OR.getProperty("otherLeadSMake")).getText();
		 * Assert.assertEquals(otherLeadSMake, Make);
		 * 
		 * String otherLeadSModel =
		 * getObject(CallCenter_OR.getProperty("otherLeadSModel")).getText();
		 * Assert.assertEquals(otherLeadSModel, Model);
		 * 
		 * String otherLeadSVariant =
		 * getObject(CallCenter_OR.getProperty("otherLeadSVariant")).getText();
		 * Assert.assertEquals(otherLeadSVariant, Variant);
		 * 
		 * String otherLeadSColor =
		 * getObject(CallCenter_OR.getProperty("otherLeadSColor")).getText();
		 * Assert.assertEquals(otherLeadSColor, Colour);
		 */

		// String otherLeadSKms =
		// getObject(RoadPrice_OR.getProperty("otherLeadSKms")).getText();
		// Assert.assertEquals(otherLeadSKms, Kilometer);

	}

	// Pop up login
	public void popupLogin() {
		explicitWait_object(CarForSale_OR.getProperty("username"));
		sendInput(CarForSale_OR.getProperty("username"), CONFIG.getProperty("websiteUser"));
		explicitWait_object(CarForSale_OR.getProperty("password"));
		sendInput(CarForSale_OR.getProperty("password"), CONFIG.getProperty("websitePassword"));
		clickOnElement(CarForSale_OR.getProperty("btnLogIn"));
	}
	// Scenario 3: Sell a car

	public void sellACarwithBuyerSeller(String Year, String Make, String Model, String Variant, String Killometer) {

		explicitWait_object(HomePage_OR.getProperty("carValSelectYear"));
		clickOnElement(HomePage_OR.getProperty("carValSelectYear"));
		WebElement yearList = getObject(HomePage_OR.getProperty("enterCarValYear"));
		yearList.sendKeys(Year);
		yearList.sendKeys(Keys.ENTER);

		explicitWait_object(HomePage_OR.getProperty("carValSelectMake"));
		clickOnElement(HomePage_OR.getProperty("carValSelectMake"));
		WebElement makeList = getObject(HomePage_OR.getProperty("enetrCarValMake"));
		makeList.sendKeys(Make);
		makeList.sendKeys(Keys.ENTER);

		explicitWait_object(HomePage_OR.getProperty("carValselectModel"));
		clickOnElement(HomePage_OR.getProperty("carValselectModel"));
		WebElement modelList = getObject(HomePage_OR.getProperty("entercarValModel"));
		modelList.sendKeys(Model);
		modelList.sendKeys(Keys.ENTER);

		explicitWait_object(HomePage_OR.getProperty("carValSelectVeriant"));
		clickOnElement(HomePage_OR.getProperty("carValSelectVeriant"));
		WebElement varientList = getObject(HomePage_OR.getProperty("entercarValVeriant"));
		varientList.sendKeys(Variant);
		varientList.sendKeys(Keys.ENTER);

		sendInput(HomePage_OR.getProperty("enterCarValKillometer"), Killometer);
		clickOnElement(HomePage_OR.getProperty("btnGo"));
	}

	// Call Center : Other lead
	public void otherLead(String LeadType, String LeadSource, String LeadCategory) throws InterruptedException {
		explicitWait_object(RoadPrice_OR.getProperty("otherLeadTab"));
		clickOnElement(RoadPrice_OR.getProperty("otherLeadTab"));
		sendInput(RoadPrice_OR.getProperty("otherLeadMob"), CONFIG.getProperty("websiteMobNo"));
        Thread.sleep(2000);
        explicitWait_object(RoadPrice_OR.getProperty("otherLeadType"));
		String actualotherLeadType = getObject(RoadPrice_OR.getProperty("otherLeadType")).getText();
		Assert.assertEquals(actualotherLeadType, LeadType);
		
		/*explicitWait_object(RoadPrice_OR.getProperty("otherLeadSource"));

		String actualotherLeadSource = getObject(RoadPrice_OR.getProperty("otherLeadSource")).getText();
		Assert.assertEquals(actualotherLeadSource, LeadSource);

		String actualotherLeadCategogy = getObject(RoadPrice_OR.getProperty("otherLeadCategory")).getText();
		Assert.assertEquals(actualotherLeadCategogy, LeadCategory);*/

		// For current date
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String systemDate = dateFormat.format(date);

		String actualotherLeadDate = getObject(RoadPrice_OR.getProperty("otherLeadDate")).getText();
		Assert.assertEquals(actualotherLeadDate, systemDate);
		explicitWait_object(RoadPrice_OR.getProperty("otherLeadMobNo"));
		String actualotherLeadMobno = getObject(RoadPrice_OR.getProperty("otherLeadMobNo")).getText();
		Assert.assertEquals(actualotherLeadMobno, CONFIG.getProperty("websiteMobNo"));
	}

	// Scenario 8: Check Exchange Price.
	public void CheckExchangeCarPrice(String Year, String Make, String Model, String Variant, String kilometer,
			String City) throws InterruptedException {

		JavascriptExecutor je = (JavascriptExecutor) driver;
		WebElement element = getObject(RoadPrice_OR.getProperty("exYear"));
		je.executeScript("arguments[0].scrollIntoView(true);", element);
		// Srelect Yea
		explicitWait_object(RoadPrice_OR.getProperty("exYear"));
		clickOnElement(RoadPrice_OR.getProperty("exYear"));
		WebElement yearList = getObject(RoadPrice_OR.getProperty("exYearEnter"));
		yearList.sendKeys(Year);
		yearList.sendKeys(Keys.ENTER);

		// Select Make
		explicitWait_object(RoadPrice_OR.getProperty("exMake"));
		clickOnElement(RoadPrice_OR.getProperty("exMake"));
		WebElement makeList = getObject(RoadPrice_OR.getProperty("exMakeEnter"));
		makeList.sendKeys(Make);
		makeList.sendKeys(Keys.ENTER);

		// Select Model
		explicitWait_object(RoadPrice_OR.getProperty("exModel"));
		clickOnElement(RoadPrice_OR.getProperty("exModel"));
		WebElement modelList = getObject(RoadPrice_OR.getProperty("exModelEnter"));
		modelList.sendKeys(Model);
		modelList.sendKeys(Keys.ENTER);

		// Select Variant
		explicitWait_object(RoadPrice_OR.getProperty("exVariant"));
		clickOnElement(RoadPrice_OR.getProperty("exVariant"));
		WebElement variantList = getObject(RoadPrice_OR.getProperty("exVariantEnter"));
		variantList.sendKeys(Variant);
		variantList.sendKeys(Keys.ENTER);

		// Send kilometer
		sendInput(RoadPrice_OR.getProperty("exKilometer"), kilometer);

		// Select City
		explicitWait_object(RoadPrice_OR.getProperty("exCity"));
		clickOnElement(RoadPrice_OR.getProperty("exCity"));
		WebElement citytList = getObject(RoadPrice_OR.getProperty("exCityEnter"));
		citytList.sendKeys(City);
		citytList.sendKeys(Keys.ENTER);

		clickOnElement(RoadPrice_OR.getProperty("btnExchange"));
		clickOnElement(RoadPrice_OR.getProperty("btnexSellYourCar"));

	}

	// Call Center : Add Lead
	public void addLead(String LeadType, String Name, String Phoneno, String Email, String State, String City,
			String MfgYear, String MfgMonth, String Make, String Model, String Variant, String Colour, String Kilometer,
			String Owner, String RegNumber, String RegCity, String RegYear, String RegMonth, String Insurance,
			String InsuranceExpiry, String LeadStatus, String ExpectedPrice, String Comments)
					throws InterruptedException {
		// getObject(CallCenter_OR.getProperty("leadTypedrop")).sendKeys(Keys.TAB);
		// selectOption(CallCenter_OR.getProperty("leadTypedrop"), LeadType);

		explicitWait_object(CallCenter_OR.getProperty("name"));
		sendInput(CallCenter_OR.getProperty("name"), Name);
		getObject(CallCenter_OR.getProperty("name")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("phoneno"));
		sendInput(CallCenter_OR.getProperty("phoneno"), Phoneno + uniqueID);
		getObject(CallCenter_OR.getProperty("phoneno")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("email"));
		String email = Email + uniqueID + "@gmail.com";
		sendInput(CallCenter_OR.getProperty("email"), email);
		// Select State
		getObject(CallCenter_OR.getProperty("email")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("selectStatedrop"));
		selectOption(CallCenter_OR.getProperty("selectStatedrop"), State);

		// Select City
		getObject(CallCenter_OR.getProperty("selectStatedrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("selectCitydrop"));

		selectOption(CallCenter_OR.getProperty("selectCitydrop"), City);

		// Select Manufacturing Year
		getObject(CallCenter_OR.getProperty("selectCitydrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("mfgYeardrop"));
		selectOption(CallCenter_OR.getProperty("mfgYeardrop"), MfgYear);

		// Select Manufacturing Month
		getObject(CallCenter_OR.getProperty("mfgYeardrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("mfgMonthdrop"));
		selectOption(CallCenter_OR.getProperty("mfgMonthdrop"), MfgMonth);

		// Select Make
		getObject(CallCenter_OR.getProperty("mfgMonthdrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("makedrop"));
		selectOption(CallCenter_OR.getProperty("makedrop"), Make);

		// Select Model
		getObject(CallCenter_OR.getProperty("makedrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("modeldrop"));
		selectOption(CallCenter_OR.getProperty("modeldrop"), Model);

		// Select Variant
		getObject(CallCenter_OR.getProperty("modeldrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("varientdrop"));
		selectOption(CallCenter_OR.getProperty("varientdrop"), Variant);

		// Select Colour
		getObject(CallCenter_OR.getProperty("varientdrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("colourdrop"));
		selectOption(CallCenter_OR.getProperty("colourdrop"), Colour);
		Thread.sleep(5000);
		getObject(CallCenter_OR.getProperty("colourdrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("kilometer"));
		sendInput(CallCenter_OR.getProperty("kilometer"), Kilometer);
		// Select Owner
		getObject(CallCenter_OR.getProperty("kilometer")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("ownerdrop"));
		selectOption(CallCenter_OR.getProperty("ownerdrop"), Owner);
		getObject(CallCenter_OR.getProperty("ownerdrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("regNo"));
		sendInput(CallCenter_OR.getProperty("regNo"), RegNumber + uniqueID);
		// Select Registration City
		getObject(CallCenter_OR.getProperty("regNo")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("regCitydrop"));
		selectOption(CallCenter_OR.getProperty("regCitydrop"), RegCity);

		// Select Registration Year
		getObject(CallCenter_OR.getProperty("regCitydrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("regYeardrop"));
		selectOption(CallCenter_OR.getProperty("regYeardrop"), RegYear);

		// Select Registration Month
		getObject(CallCenter_OR.getProperty("regYeardrop")).sendKeys(Keys.TAB);
		explicitWait_object(CallCenter_OR.getProperty("regMonthdrop"));
		selectOption(CallCenter_OR.getProperty("regMonthdrop"), RegMonth);

		// Select Insurance
		explicitWait_object(CallCenter_OR.getProperty("insurancedrop"));
		selectOption(CallCenter_OR.getProperty("insurancedrop"), Insurance);

		// Select Insurance Expiry Date
		explicitWait_object(CallCenter_OR.getProperty("insuranceExpiry"));
		// clickOnElement(CallCenter_OR.getProperty("insuranceExpiry"));
		sendInput(CallCenter_OR.getProperty("insuranceExpiry"), InsuranceExpiry);

		// Select Lead Status
		explicitWait_object(CallCenter_OR.getProperty("leadStatusdrop"));
		selectOption(CallCenter_OR.getProperty("leadStatusdrop"), LeadStatus);
		Thread.sleep(5000);
		explicitWait_object(CallCenter_OR.getProperty("expectedPrice"));
		sendInput(CallCenter_OR.getProperty("expectedPrice"), ExpectedPrice);
		explicitWait_object(CallCenter_OR.getProperty("comments"));
		sendInput(CallCenter_OR.getProperty("comments"), Comments);
		clickOnElement(CallCenter_OR.getProperty("btnAdd"));

	}

	// Invalid email without domain name
	public void verifyEmailID(String Email, String ValidationMsg) {
		sendInput(CallCenter_OR.getProperty("email"), Email);
		clickOnElement(CallCenter_OR.getProperty("emailClick"));
		String getEmail = getObject(CallCenter_OR.getProperty("emailIdVaidationMsg")).getText();
		Assert.assertEquals(getEmail, ValidationMsg);
	}

	public void VerifyInvalidEMail(String Email, String ValidationMsg) {
		explicitWait_object(CallCenter_OR.getProperty("email"));
		sendInput(CallCenter_OR.getProperty("email"), Email);
		clickOnElement(CallCenter_OR.getProperty("emailClick"));
		explicitWait_object(CallCenter_OR.getProperty("emailIdVaidationMsg"));
		String getEmail = getObject(CallCenter_OR.getProperty("emailIdVaidationMsg")).getText();
		Assert.assertEquals(getEmail, ValidationMsg);
	}

	public void verifyMobNo(String Mob) {
		explicitWait_object(CallCenter_OR.getProperty("phoneno"));
		sendInput(CallCenter_OR.getProperty("phoneno"), Mob);
		// driver.findElement(By.xpath(".//*[@id='add_product_lead']/div[1]/div/div/div[3]/div[2]")).click();
		clickOnElement(CallCenter_OR.getProperty("pnNumberClick"));
		explicitWait_object(CallCenter_OR.getProperty("phNumberValidation"));
		Assert.assertEquals(getObject(CallCenter_OR.getProperty("phNumberValidation")).isDisplayed(), true,
				"Mandetary Meassage not display for Ph number");
	}

	// Dealer Login
	public void dealerLogin() {
		driver.get(CONFIG.getProperty("dealerURL"));
		driver.manage().timeouts().implicitlyWait((3000), TimeUnit.SECONDS);
		sendInput(StockSection_OR.getProperty("dealerUname"), CONFIG.getProperty("dealerUname"));
		sendInput(StockSection_OR.getProperty("dealerPass"), CONFIG.getProperty("dealerPass"));
		clickOnElement(StockSection_OR.getProperty("btnlogin"));

	}

	// Dealer Login -Vin check search
	public void searchVincheck(String Chassisno) {
		clickOnElement(ToolSection_OR.getProperty("vinchecklink"));
		String FormText = getObject(ToolSection_OR.getProperty("vincheck")).getText();
		Assert.assertEquals(FormText, "VIN CHECK");
		explicitWait_object(ToolSection_OR.getProperty("chassisnumbertext"));
		sendInput(ToolSection_OR.getProperty("chassisnumbertext"), Chassisno);
		explicitWait_object(ToolSection_OR.getProperty("btnSearch"));
		clickOnElement(ToolSection_OR.getProperty("btnSearch"));
		explicitWait_object(ToolSection_OR.getProperty("getYear"));
		String getYearText = getObject(ToolSection_OR.getProperty("getYear")).getText();
		Assert.assertEquals(getYearText, "2013");
		explicitWait_object(ToolSection_OR.getProperty("getMonth"));
		String getMonthText = getObject(ToolSection_OR.getProperty("getMonth")).getText();
		Assert.assertEquals(getMonthText, "FEB");
		explicitWait_object(ToolSection_OR.getProperty("getMake"));
		String getMakeText = getObject(ToolSection_OR.getProperty("getMake")).getText();
		Assert.assertEquals(getMakeText, "HYUNDAI");
		explicitWait_object(ToolSection_OR.getProperty("getModel"));
		String getModelText = getObject(ToolSection_OR.getProperty("getModel")).getText();
		Assert.assertEquals(getModelText, "EON");
	}

	public void navigateToResidualsTab() {
		explicitWait_object(ToolSection_OR.getProperty("ToolsDrop"));
		clickOnElement(ToolSection_OR.getProperty("ToolsDrop"));
		explicitWait_object(ToolSection_OR.getProperty("residulalink"));
		clickOnElement(ToolSection_OR.getProperty("residulalink"));
		String getTitle = getObject(ToolSection_OR.getProperty("residualTitle")).getText();
		Assert.assertEquals(getTitle, "RESIDUALS");
	}

	public void verifyLHSMenu() {
		// Verify main menu tabs
		String mainTabDashboard = getObject(StockSection_OR.getProperty("mainTabDashboard")).getText();
		Assert.assertEquals(mainTabDashboard, "Dashboard");

		String mainTabMyStock = getObject(StockSection_OR.getProperty("mainTabMyStock")).getText();
		Assert.assertEquals(mainTabMyStock, "My Stock");

		String mainTabLeads = getObject(StockSection_OR.getProperty("mainTabLeads")).getText();
		Assert.assertEquals(mainTabLeads, "Leads");

		String mainTabIBBTrade = getObject(StockSection_OR.getProperty("mainTabIBBTrade")).getText();
		Assert.assertEquals(mainTabIBBTrade, "IBB Trade");

		String mainTabMyAccount = getObject(StockSection_OR.getProperty("mainTabMyAccount")).getText();
		Assert.assertEquals(mainTabMyAccount, "My Account");

		String mainTabTools = getObject(StockSection_OR.getProperty("mainTabTools")).getText();
		Assert.assertEquals(mainTabTools, "Tools");

		String mainTabOthers = getObject(StockSection_OR.getProperty("mainTabOthers")).getText();
		Assert.assertEquals(mainTabOthers, "Others");

		String mainTabReports = getObject(StockSection_OR.getProperty("mainTabReports")).getText();
		Assert.assertEquals(mainTabReports, "Reports");

		String mainTabMyPrivateLeads = getObject(StockSection_OR.getProperty("mainTabMyPrivateLeads")).getText();
		Assert.assertEquals(mainTabMyPrivateLeads, "My Private Leads");

		// Verify Sub menu tabs- My Stock

		clickOnElement(StockSection_OR.getProperty("myStockdrop"));

		String subMenuManage = getObject(StockSection_OR.getProperty("subMenuManage")).getText();
		Assert.assertEquals(subMenuManage, "Manage");

		String subMenuOffers = getObject(StockSection_OR.getProperty("subMenuOffers")).getText();
		Assert.assertEquals(subMenuOffers, "Offers");

		String subMenuHistory = getObject(StockSection_OR.getProperty("subMenuHistory")).getText();
		Assert.assertEquals(subMenuHistory, "History");

	}

	// Dealer Login
	public void navigateToMangeStockTab() {
		clickOnElement(StockSection_OR.getProperty("manageLink"));
		explicitWait_object(StockSection_OR.getProperty("mangestockTitle"));
		String getStockTitle = getObject(StockSection_OR.getProperty("mangestockTitle")).getText();
		Assert.assertEquals(getStockTitle, "MANAGE STOCKS");

		explicitWait_object(StockSection_OR.getProperty("btnAddStock"));
		clickOnElement(StockSection_OR.getProperty("btnAddStock"));
		windowHandling();
	}
	// Dealer Login-Add Stock

	public void dealerAddStock(String StockStatus, String StockCategory, String MfgYear, String MfgMonth, String Make,
			String Model, String Variant, String Colour, String Kilometer, String Owner, String RegNumber,
			String RegCity, String RegYear, String RegMonth, String Insurance, String InsuranceExpiry, String ChassisNo,
			String EngineNo, String CertifiedBy, String AboutStock, String SellingPrice, String DealerPrice)
					throws InterruptedException {
		// Add New Stock
		clickOnElement(StockSection_OR.getProperty("rdiobtnPrivate"));
		// Stock Status
		getObject(StockSection_OR.getProperty("rdiobtnPrivate")).sendKeys(Keys.TAB);
		explicitWait_object(StockSection_OR.getProperty("dropStockStatus"));
		selectOption(StockSection_OR.getProperty("dropStockStatus"), StockStatus);
		// Stock Category
		getObject(StockSection_OR.getProperty("dropStockStatus")).sendKeys(Keys.TAB);
		explicitWait_object(StockSection_OR.getProperty("dropStockCategory"));
		
		selectOption(StockSection_OR.getProperty("dropStockCategory"), StockCategory);
		getObject(StockSection_OR.getProperty("dropStockCategory")).sendKeys(Keys.TAB);
		// Mfg Year
		explicitWait_object(StockSection_OR.getProperty("dropMfgYear"));
		selectOption(StockSection_OR.getProperty("dropMfgYear"), MfgYear);
		getObject(StockSection_OR.getProperty("dropMfgYear")).sendKeys(Keys.TAB);
		// Mfg Month
		explicitWait_object(StockSection_OR.getProperty("dropMfgMonth"));
		selectOption(StockSection_OR.getProperty("dropMfgMonth"), MfgMonth);
		getObject(StockSection_OR.getProperty("dropMfgMonth")).sendKeys(Keys.TAB);
		// Make
		explicitWait_object(StockSection_OR.getProperty("dropMake"));
		selectOption(StockSection_OR.getProperty("dropMake"), Make);
		getObject(StockSection_OR.getProperty("dropMake")).sendKeys(Keys.TAB);
		// Model
		explicitWait_object(StockSection_OR.getProperty("dropModel"));
		selectOption(StockSection_OR.getProperty("dropModel"), Model);
		getObject(StockSection_OR.getProperty("dropModel")).sendKeys(Keys.TAB);
		// Variant
		explicitWait_object(StockSection_OR.getProperty("dropVariant"));
		selectOption(StockSection_OR.getProperty("dropVariant"), Variant);
		getObject(StockSection_OR.getProperty("dropVariant")).sendKeys(Keys.TAB);
		// Colour
		explicitWait_object(StockSection_OR.getProperty("dropColour"));
		selectOption(StockSection_OR.getProperty("dropColour"), Colour);
		getObject(StockSection_OR.getProperty("dropColour")).sendKeys(Keys.TAB);
		// Kilometers
		sendInput(StockSection_OR.getProperty("stockkilometer"), Kilometer);
		getObject(StockSection_OR.getProperty("stockkilometer")).sendKeys(Keys.TAB);
		// Owner
		explicitWait_object(StockSection_OR.getProperty("dropOwner"));
		selectOption(StockSection_OR.getProperty("dropOwner"), Owner);
		getObject(StockSection_OR.getProperty("dropOwner")).sendKeys(Keys.TAB);
		// Reg Number
		sendInput(StockSection_OR.getProperty("stockRegNo"), RegNumber + uniqueID);
		getObject(StockSection_OR.getProperty("stockRegNo")).sendKeys(Keys.TAB);
		// Reg City
		explicitWait_object(StockSection_OR.getProperty("dropRegCity"));
		selectOption(StockSection_OR.getProperty("dropRegCity"), RegCity);
		getObject(StockSection_OR.getProperty("dropRegCity")).sendKeys(Keys.TAB);
		// Reg Year
		explicitWait_object(StockSection_OR.getProperty("dropRegYear"));
		selectOption(StockSection_OR.getProperty("dropRegYear"), RegYear);
		getObject(StockSection_OR.getProperty("dropRegYear")).sendKeys(Keys.TAB);
		// Reg Month
		explicitWait_object(StockSection_OR.getProperty("dropRegMonth"));
		selectOption(StockSection_OR.getProperty("dropRegMonth"), RegMonth);
		getObject(StockSection_OR.getProperty("dropRegMonth")).sendKeys(Keys.TAB);
		// Insurance
		explicitWait_object(StockSection_OR.getProperty("dropInsurance"));
		selectOption(StockSection_OR.getProperty("dropInsurance"), Insurance);
		// Insurance Exp Date
		explicitWait_object(StockSection_OR.getProperty("insuranceExpDate"));
		sendInput(StockSection_OR.getProperty("insuranceExpDate"), InsuranceExpiry);
		// Chassis Number
		sendInput(StockSection_OR.getProperty("chassisNo"), ChassisNo + uniqueID);
		// Engine Number
		sendInput(StockSection_OR.getProperty("engineNo"), EngineNo + uniqueID);
		// Certified By
		explicitWait_object(StockSection_OR.getProperty("dropCertifiedBy"));
		selectOption(StockSection_OR.getProperty("dropCertifiedBy"), CertifiedBy);
		// About Stock
		sendInput(StockSection_OR.getProperty("aboutStock"), AboutStock);
		// Selling Price
		sendInput(StockSection_OR.getProperty("sellingPrice"), SellingPrice);
		// Dealer Price
		sendInput(StockSection_OR.getProperty("dealerPrice"), DealerPrice);
		explicitWait_object(StockSection_OR.getProperty("btnviewIBBprice"));
		clickOnElement(StockSection_OR.getProperty("btnviewIBBprice"));
		explicitWait_object(StockSection_OR.getProperty("btnAdd"));
		clickOnElement(StockSection_OR.getProperty("btnAdd"));
		Thread.sleep(5000);
	}

	// Dealer login - Edit Stock

	public void dealerEditStock(String StockStatus, String MfgYear, String MfgMonth, String Make, String Model,
			String Variant, String Colour, String Kilometer, String Owner, String RegNumber, String RegCity,
			String RegYear, String RegMonth, String Insurance, String CertifiedBy, String SellingPrice,
			String DealerPrice) throws InterruptedException {

		// Stock Status
		explicitWait_object(StockSection_OR.getProperty("dropStockStatus"));
		clickOnElement(StockSection_OR.getProperty("dropStockStatus"));
		selectOption(StockSection_OR.getProperty("dropStockStatus"), StockStatus);
		// Mfg Year
		explicitWait_object(StockSection_OR.getProperty("dropMfgYear"));
		clickOnElement(StockSection_OR.getProperty("dropMfgYear"));
		selectOption(StockSection_OR.getProperty("dropMfgYear"), MfgYear);
		// Mfg Month
		explicitWait_object(StockSection_OR.getProperty("dropMfgMonth"));
		clickOnElement(StockSection_OR.getProperty("dropMfgMonth"));
		selectOption(StockSection_OR.getProperty("dropMfgMonth"), MfgMonth);
		// Make
		explicitWait_object(StockSection_OR.getProperty("dropMake"));
		clickOnElement(StockSection_OR.getProperty("dropMake"));
		selectOption(StockSection_OR.getProperty("dropMake"), Make);
		// Model
		explicitWait_object(StockSection_OR.getProperty("dropModel"));
		clickOnElement(StockSection_OR.getProperty("dropModel"));
		selectOption(StockSection_OR.getProperty("dropModel"), Model);
		// Variant
		explicitWait_object(StockSection_OR.getProperty("dropVariant"));
		clickOnElement(StockSection_OR.getProperty("dropVariant"));
		selectOption(StockSection_OR.getProperty("dropVariant"), Variant);
		// Colour
		explicitWait_object(StockSection_OR.getProperty("dropColour"));
		clickOnElement(StockSection_OR.getProperty("dropColour"));
		selectOption(StockSection_OR.getProperty("dropColour"), Colour);
		// Kilometers
		sendInput(StockSection_OR.getProperty("stockkilometer"), Kilometer);
		// Owner
		explicitWait_object(StockSection_OR.getProperty("dropOwner"));
		clickOnElement(StockSection_OR.getProperty("dropOwner"));
		selectOption(StockSection_OR.getProperty("dropOwner"), Owner);
		// Reg Number
		sendInput(StockSection_OR.getProperty("stockRegNo"), RegNumber);
		// Reg City
		explicitWait_object(StockSection_OR.getProperty("dropRegCity"));
		clickOnElement(StockSection_OR.getProperty("dropRegCity"));
		selectOption(StockSection_OR.getProperty("dropRegCity"), RegCity);
		// Reg Year
		explicitWait_object(StockSection_OR.getProperty("dropRegYear"));
		clickOnElement(StockSection_OR.getProperty("dropRegYear"));
		selectOption(StockSection_OR.getProperty("dropRegYear"), RegYear);
		// Reg Month
		explicitWait_object(StockSection_OR.getProperty("dropRegMonth"));
		clickOnElement(StockSection_OR.getProperty("dropRegMonth"));
		selectOption(StockSection_OR.getProperty("dropRegMonth"), RegMonth);
		// Insurance
		explicitWait_object(StockSection_OR.getProperty("dropInsurance"));
		clickOnElement(StockSection_OR.getProperty("dropInsurance"));
		selectOption(StockSection_OR.getProperty("dropInsurance"), Insurance);
		// Certified By
		explicitWait_object(StockSection_OR.getProperty("dropCertifiedBy"));
		clickOnElement(StockSection_OR.getProperty("dropCertifiedBy"));
		selectOption(StockSection_OR.getProperty("dropCertifiedBy"), CertifiedBy);
		// Selling Price
		sendInput(StockSection_OR.getProperty("sellingPrice"), SellingPrice);
		// Dealer Price
		sendInput(StockSection_OR.getProperty("dealerPrice"), DealerPrice);

		explicitWait_object(StockSection_OR.getProperty("btnviewIBBprice"));
		clickOnElement(StockSection_OR.getProperty("btnviewIBBprice"));
		explicitWait_object(StockSection_OR.getProperty("btnUpdate"));
		clickOnElement(StockSection_OR.getProperty("btnUpdate"));
	}

	// Dealer login-Search stock by Make 
	public void searchMakeStock(String Make) throws InterruptedException

	{
		explicitWait_object(StockSection_OR.getProperty("makeField"));
		sendInput(StockSection_OR.getProperty("makeField"), Make);
		Thread.sleep(9000);
		List<WebElement> list = driver
				.findElements(By.xpath(".//*[@id='container']//div[1]/div/div[2]//div[4]//table/tbody//td[2]"));
		int count = list.size();
		for (int i = 0; i < count; i++) {
			String actualMake = list.get(i).getText();
			if (actualMake.equalsIgnoreCase(Make)) {
				Assert.assertEquals(actualMake, Make);
			} else {
				Assert.assertEquals(getObject(StockSection_OR.getProperty("UnavailableStock")).isDisplayed(), true,
						"Displaying wrong result");
			}
		}

		
driver.navigate().refresh();
	}
	//Dealer login-Search Stock By Model
	public void searchModelStock(String Model) throws InterruptedException

	{
		explicitWait_object(StockSection_OR.getProperty("modelField"));
		sendInput(StockSection_OR.getProperty("modelField"), Model);
		Thread.sleep(9000);
		List<WebElement> list = driver
				.findElements(By.xpath(".//*[@id='container']//div[1]/div/div[2]//div[4]//table/tbody//td[3]"));
		int count = list.size();
		for (int i = 0; i < count; i++) {
			String actualMake = list.get(i).getText();
			if (actualMake.equalsIgnoreCase(Model)) {
				Assert.assertEquals(actualMake, Model);
			} else {
				Assert.assertEquals(getObject(StockSection_OR.getProperty("UnavailableStock")).isDisplayed(), true,
						"Displaying wrong result");
			}
		}

		driver.navigate().refresh();
	}

	//Dealer login-Search Stock By Variant
		public void searchVarientStock(String Variant) throws InterruptedException

		{
			explicitWait_object(StockSection_OR.getProperty("variantField"));
			sendInput(StockSection_OR.getProperty("variantField"), Variant);
			Thread.sleep(9000);
			List<WebElement> list = driver
					.findElements(By.xpath(".//*[@id='container']//div[1]/div/div[2]//div[4]//table/tbody//td[4]"));
			int count = list.size();
			for (int i = 0; i < count; i++) {
				String actualMake = list.get(i).getText();
				if (actualMake.equalsIgnoreCase(Variant)) {
					Assert.assertEquals(actualMake, Variant);
				} else {
					Assert.assertEquals(getObject(StockSection_OR.getProperty("UnavailableStock")).isDisplayed(), true,
							"Displaying wrong result");
				}
			}
			driver.navigate().refresh();
		}
		
		//Dealer login-Search Stock By Variant
		public void searchUnavaibleStock() throws InterruptedException
		{
					explicitWait_object(StockSection_OR.getProperty("variantField"));
					sendInput(StockSection_OR.getProperty("variantField"), "ABC");
					Thread.sleep(9000);
					
							Assert.assertEquals(getObject(StockSection_OR.getProperty("UnavailableStock")).isDisplayed(), true,
									"Displaying wrong result");
						
					driver.navigate().refresh();
				}
	// Dealer Login - Vin Check Button
	public void stockVincheck(String Chassisno) throws InterruptedException {
		explicitWait_object(StockSection_OR.getProperty("btnVinCheck"));
		clickOnElement(StockSection_OR.getProperty("btnVinCheck"));
		Thread.sleep(2000);
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
	    explicitWait_object(StockSection_OR.getProperty("vinChassisNoField"));
		clickOnElement(StockSection_OR.getProperty("vinChassisNoField"));
		sendInput(StockSection_OR.getProperty("vinChassisNoField"), Chassisno);
		explicitWait_object(StockSection_OR.getProperty("btnSearchVin"));
		clickOnElement(StockSection_OR.getProperty("btnSearchVin"));
		explicitWait_object(StockSection_OR.getProperty("getYear"));
		String getYearText = getObject(StockSection_OR.getProperty("getYear")).getText();
		Assert.assertEquals(getYearText, "2013");
		explicitWait_object(StockSection_OR.getProperty("getMonth"));
		String getMonthText = getObject(StockSection_OR.getProperty("getMonth")).getText();
		Assert.assertEquals(getMonthText, "FEB");
		explicitWait_object(StockSection_OR.getProperty("getMake"));
		String getMakeText = getObject(StockSection_OR.getProperty("getMake")).getText();
		Assert.assertEquals(getMakeText, "HYUNDAI");
		explicitWait_object(StockSection_OR.getProperty("getModel"));
		String getModelText = getObject(StockSection_OR.getProperty("getModel")).getText();
		Assert.assertEquals(getModelText, "EON");
	    driver.close();
	    driver.switchTo().window(tabs2.get(0));
		explicitWait_object(StockSection_OR.getProperty("closeVinCheckPopUp"));
		clickOnElement(StockSection_OR.getProperty("closeVinCheckPopUp"));
		
	}

	// Excel Reader
	public static String[][] getTableArray(String xlFilePath, String sheetName, String tableName) {
		String[][] tabArray = null;
		try {

			Workbook workbook = Workbook.getWorkbook(new File(xlFilePath));
			Sheet sheet = workbook.getSheet(sheetName);
			int startRow, startCol, endRow, endCol, ci, cj;
			Cell tableStart = sheet.findCell(tableName);
			startRow = tableStart.getRow();
			startCol = tableStart.getColumn();
			Cell tableEnd = sheet.findCell(tableName, startCol + 1, startRow + 1, 100, 64000, false);
			endRow = tableEnd.getRow();
			endCol = tableEnd.getColumn();
			tabArray = new String[endRow - startRow - 1][endCol - startCol - 1];
			ci = 0;
			for (int i = startRow + 1; i < endRow; i++, ci++) {
				cj = 0;
				for (int j = startCol + 1; j < endCol; j++, cj++) {
					tabArray[ci][cj] = sheet.getCell(j, i).getContents();
				}
			}
			APP_LOGS.debug("get the data from the excel sheet with the tablename" + tableEnd);
		} catch (Exception e) {
			System.out.println("error in getTableArray()");
		}

		return (tabArray);
	}
}
